<?php

declare(strict_types=1);
use Everkit\Framework\Everkit;

require '../vendor/autoload.php';

$appPath = __DIR__ . '/../';
define('APP_PATH', $appPath);
$config = require $appPath . 'app/Config/web.php';
$webLocalPath = $appPath . 'app/Config/web.local.php';

if (is_file($webLocalPath)) {
    $config = array_merge_recursive($config, require $webLocalPath);
}

$app = Everkit::app();
$app->setConfig($config);
$app->run();
