<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 01.09.17
 * Time: 14:01
 */

require './vendor/autoload.php';

use Everkit\Framework\Everkit;

$appPath = __DIR__ . '/';
define('APP_PATH', $appPath);
$config = require $appPath . 'app/Config/console.php';

$app = Everkit::app();
$app->setConfig($config);
$app->run();
