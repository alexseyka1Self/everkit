<?php

namespace Everkit\Base\Migrations;

use Everkit\Framework\Classes\Db\CMigration;
use Everkit\Base\Models\Author;
use Everkit\Base\Models\Post;

class m20170906_154408_add_test_posts_and_authors extends CMigration
{
    public function up(): bool
    {
        // Adding a first author
        $author = new Author();
        $author->firstname = "Steve";
        $author->lastname = "Jobs";
        $author->save();
        $lastID = $author->getLastId();
        // Adding posts to this author
        $post = new Post();
        $post->title = "Lorem ipsum dolor sit amet, consectetur adipiscing elit";
        $post->text = "Curabitur eu faucibus odio. Vivamus interdum maximus nibh a varius. Sed eu turpis maximus, aliquet nunc sed, convallis eros. Maecenas rhoncus turpis ac sem ultricies, in bibendum mi pretium. Nunc ac aliquam ante, at porta metus. Aenean orci nulla, fermentum a dignissim ac, efficitur et metus. Nulla eu dolor a nisi finibus aliquam vel et arcu. In hendrerit tempor nisi id scelerisque. Curabitur at turpis quis quam faucibus ultrices. Donec sed sollicitudin ex. Fusce ornare sed urna vel hendrerit. Aenean sed lobortis risus, a fringilla neque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla facilisi.";
        $post->author_id = $lastID;
        $post->save();

        $post->title = "Curabitur dapibus malesuada libero fermentum congue";
        $post->text = "Integer ornare nisl sit amet urna euismod elementum. Curabitur interdum dolor condimentum mattis laoreet. Fusce maximus dignissim urna, sit amet tincidunt justo luctus et. Mauris rhoncus ligula vel lorem tempus, eu euismod lacus viverra. Quisque sodales, mi non gravida porta, ante odio sagittis erat, nec pharetra dui erat non odio. Fusce mi ante, lobortis et consectetur non, ultricies eu lorem.";
        $post->author_id = $lastID;
        $post->save();

        $post->title = "Curabitur ipsum ex, iaculis a mattis et, luctus quis urna";
        $post->text = "Aliquam erat volutpat. Aenean elementum suscipit magna et venenatis. Aliquam in erat nulla. Sed et neque laoreet, blandit ligula sed, tempor leo. Sed suscipit commodo sem, at fringilla ante tempor ac. Proin fermentum vel enim eu varius. Vivamus id fermentum justo. In hac habitasse platea dictumst. Phasellus sagittis ex lacus, ac ullamcorper purus efficitur eu. Fusce lacinia, libero at efficitur cursus, massa velit condimentum lorem, eu maximus mauris urna id eros.";
        $post->author_id = $lastID;
        $post->save();

        // Adding a second author
        $author->firstname = "Elon Reeve";
        $author->lastname = "Musk";
        $author->save();
        $lastID = $author->getLastId();
        // Adding posts to this author
        $post->title = "Maecenas ullamcorper nec risus sed vestibulum";
        $post->text = "Sed in facilisis massa. Mauris molestie elit ut leo viverra pellentesque. In sit amet laoreet metus. Curabitur quis nisl nec erat blandit tincidunt. Praesent fringilla mi et euismod pharetra. Pellentesque suscipit vehicula mauris ac interdum. Sed efficitur a mauris vitae vehicula. Proin quis sem libero. Nullam venenatis porttitor laoreet. Etiam blandit felis sit amet rhoncus rhoncus. Sed in varius augue, ut dapibus ligula. Nam mollis in mauris nec porta. Donec sit amet sem et erat pellentesque hendrerit ut non leo. Morbi sollicitudin rutrum lacus at tempor. Ut tempus orci et tellus vestibulum mollis.";
        $post->author_id = $lastID;
        $post->save();

        $post->title = "Pellentesque mattis accumsan orci";
        $post->text = "Nam urna sem, fringilla eu molestie non, volutpat eu ligula. Suspendisse sed augue et augue suscipit rhoncus non eu velit. Quisque molestie tortor vel turpis tempus vulputate. Vivamus et mi elit. Nulla facilisi. Aliquam condimentum, nisi eget elementum dapibus, orci erat iaculis ipsum, non lobortis tellus erat at ex. Ut consequat tellus accumsan ipsum placerat venenatis. Pellentesque euismod convallis felis a auctor. Integer rutrum diam et diam dapibus mollis. Curabitur arcu quam, gravida id augue sed, volutpat venenatis augue. Quisque quis sapien et lorem feugiat auctor hendrerit a diam. Vivamus nunc urna, volutpat eu ante a, aliquet sodales erat. Aliquam a ipsum non tellus venenatis efficitur at mollis nisl. Integer semper mauris nisl, non tincidunt sapien bibendum sit amet.";
        $post->author_id = $lastID;
        $post->save();

        // Adding a third author
        $author->firstname = "William Henry";
        $author->lastname = "Gates";
        $author->save();
        $lastID = $author->getLastId();
        // Adding posts to this author
        $post->title = "Nam urna sem, fringilla eu molestie non, volutpat eu ligula";
        $post->text = "Sed in facilisis massa. Mauris molestie elit ut leo viverra pellentesque. In sit amet laoreet metus. Curabitur quis nisl nec erat blandit tincidunt. Praesent fringilla mi et euismod pharetra. Pellentesque suscipit vehicula mauris ac interdum. Sed efficitur a mauris vitae vehicula. Proin quis sem libero. Nullam venenatis porttitor laoreet. Etiam blandit felis sit amet rhoncus rhoncus. Sed in varius augue, ut dapibus ligula. Nam mollis in mauris nec porta. Donec sit amet sem et erat pellentesque hendrerit ut non leo. Morbi sollicitudin rutrum lacus at tempor. Ut tempus orci et tellus vestibulum mollis.";
        $post->author_id = $lastID;
        $post->save();

        return true;
    }

    public function down(): bool
    {
        // TODO: Change the autogenerated stub
        echo "m20170906_154408_add_test_posts_and_authors not support method down()" . PHP_EOL;
        return true;
    }
}