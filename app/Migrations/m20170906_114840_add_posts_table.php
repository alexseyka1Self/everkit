<?php

namespace Everkit\Base\Migrations;

use Everkit\Framework\Classes\Db\CMigration;

class m20170906_114840_add_posts_table extends CMigration
{
    public function up(): bool
    {
        $create = $this->createTable("posts", [
            'id' => "int(11) NOT NULL AUTO_INCREMENT",
            'title' => "varchar(512) NOT NULL",
            'text' => "text NOT NULL",
            'author_id' => "int(11) DEFAULT NULL"
        ], "id");
        return $create;
    }

    public function down(): bool
    {
        $this->dropTable("posts");
        return true;
    }
}