<?php

namespace Everkit\Base\Migrations;

use Everkit\Framework\Classes\Db\CMigration;

class m20170906_144350_add_authors_table extends CMigration
{
    public function up(): bool
    {
        $create = $this->createTable("authors", [
            'id' => "int(11) NOT NULL AUTO_INCREMENT",
            'firstname' => "varchar(255) NOT NULL",
            'lastname' => "varchar(255) DEFAULT NULL"
        ], "id");
        return $create;
    }

    public function down(): bool
    {
        $this->dropTable("authors");
        return true;
    }
}