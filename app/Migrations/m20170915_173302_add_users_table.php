<?php

/**
 * Created by Everkit Generator.
 * Date: {{ DATE }}
 * Time: {{ TIME }}
 */

namespace Everkit\Base\Migrations;

use Everkit\Framework\Classes\Db\CMigration;

class m20170915_173302_add_users_table extends CMigration
{
    public function up(): bool
    {
        $this->createTable('users', [
            'id' => 'int(11) NOT NULL AUTO_INCREMENT',
            'login' => 'varchar(255) NOT NULL',
            'password' => 'varchar(511) NOT NULL'
        ], 'id');
        $this->getDb()->createCommand('INSERT INTO `users` (login, password) VALUES (:login, :password)')
            ->bindParams([
                ':login' => 'admin',
                ':password' => '$2y$13$t.5qAozEeeDi/YlcbY9lkOTXZpd7xtPLmRIh366IyJnxaC/NRniyq' // hash of 'admin' word
            ])->execute();
        return true;
    }

    public function down(): bool
    {
        $this->dropTable('users');
        return true;
    }
}