<?php

use Everkit\Framework\Classes\Components\CCommandsHandler;
use Everkit\Framework\Classes\Db\CDataBase;
use Everkit\Framework\Classes\Db\PdoDrivers;

ini_set("display_errors", 1);
error_reporting(E_ALL);

return [
    'app_name' => 'Simple Application',
    'version' => 1.0,
    'components' => [
        'Db' => [
            'class' => CDataBase::class,
            'dsn' => PdoDrivers::PDO_MYSQL . ':host=localhost;dbname=framework;charset=utf8',
            'user' => 'root',
            'password' => '',
        ],
        'RequestHandler' => null,
        'Template' => null,
        'RouterHandler' => null,
        'CommandsHandler' => [
            'class' => CCommandsHandler::class,
        ]
    ],
    'params' => []
];
