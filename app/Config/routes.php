<?php

use Everkit\Base\Controllers\SiteController;

return [
    '/' => SiteController::class . '@index',
    '/posts' => SiteController::class . '@posts',
    '/post/[i:id]' => SiteController::class . '@postView',
    '/author/[i:id]' => SiteController::class . '@authorView',

    '/admin' => \Everkit\Base\Modules\Admin\Controllers\DefaultController::class . '@index',
    '/admin/login' => \Everkit\Base\Modules\Admin\Controllers\DefaultController::class . '@login',
    '/admin/logout' => \Everkit\Base\Modules\Admin\Controllers\DefaultController::class . '@logout',
    '/admin/edit-post/[i:id]' => \Everkit\Base\Modules\Admin\Controllers\DefaultController::class . '@edit',
    '/admin/new-post' => \Everkit\Base\Modules\Admin\Controllers\DefaultController::class . '@newPost',
];
