<?php

ini_set("display_errors", 1);
error_reporting(E_ALL);

return [
    'app_name' => 'Simple Application',
    'version' => 1.0,
    'components' => [
        'Db' => [
            'class' => \Everkit\Framework\Classes\Db\CDataBase::class,
            'dsn' => \Everkit\Framework\Classes\Db\PdoDrivers::PDO_MYSQL
                . ':host=localhost;dbname=framework;charset=utf8',
//            'user' => 'root',
//            'password' => '',
        ],
        'RouterHandler' => [
            'routes' => include 'routes.php'
        ],
    ],
    'widgets' => [
        'pagination' => \Everkit\Framework\Widgets\Pagination::class
    ],
    'params' => [
        'test' => ['Hello', 'my', 'little', 'friends']
    ]
];
