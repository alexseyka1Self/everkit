<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 03.05.17
 * Time: 17:43
 */

namespace Everkit\Base\Models;

use Everkit\Framework\Classes\Db\CActiveModel;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class Post
 * @package App\Models
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property integer $author_id
 */
class Post extends CActiveModel
{
    public $id;
    public $title;
    public $text;
    public $author_id;

    public function validators(): array
    {
        return [
            'title' => [
                new NotBlank(),
                new Length(['min' => 10])
            ],
            'text' => [
                new NotBlank(),
                new Length(['min' => 10])
            ]
        ];
    }

    public function columnNames(): array
    {
        return [
            'title' => 'Title of article',
            'text' => 'Text of article'
        ];
    }

    public function relations()
    {
        return [
            'author' => [self::HAS_ONE, Author::class, ['author_id' => 'id']]
        ];
    }
}
