<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 03.05.17
 * Time: 17:43
 */

namespace Everkit\Base\Models;

use Everkit\Framework\Classes\Db\CActiveModel;

/**
 * Class Post
 * @package App\Models
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property integer $author_id
 */
class Author extends CActiveModel
{
    public $id;
    public $firstname;
    public $lastname;

    public function relations()
    {
        return [
            'posts' => [self::HAS_MANY, Post::class, ['id' => 'author_id']]
        ];
    }

    public function getName()
    {
        return implode(" ", [$this->firstname, $this->lastname]);
    }
}
