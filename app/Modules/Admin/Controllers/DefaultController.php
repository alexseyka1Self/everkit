<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 06.09.17
 * Time: 17:44
 */

namespace Everkit\Base\Modules\Admin\Controllers;

use Everkit\Framework\Classes\Abstracts\ControllerAbstract;
use Everkit\Framework\Classes\UserIdentity;
use Everkit\Framework\Interfaces\ISession;
use Everkit\Framework\Models\User;
use Everkit\Base\Models\Author;
use Everkit\Base\Models\Post;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends ControllerAbstract
{
    public function index()
    {
        $posts = Post::model()->findAll();
        $response = new Response(
            $this->view->render('default/index', [
                'posts' => $posts
            ])
        );
        $response->send();
    }

    public function login()
    {
        $security = $this->app->getComponent('security');
        $request = $this->app->getComponent('request');
        $session = $this->app->getComponent('session');
        $loginForm = $request->getPost('LoginForm');
        $formToken = $request->getPost('_csrf_token') ?? null;
        $login = $loginForm['login'] ?? null;
        $password = $loginForm['password'] ?? null;
        $csrf = $security->getCsrfToken();
        if (empty($loginForm)) {
            echo $this->view->render('default/login', [
                '_csrf_token' => $csrf
            ]);
        } else {
            if ($security->checkCsrfToken($formToken)) {
                $userIdentity = new UserIdentity($login, $password);
                try {
                    if (User::checkUserIdentity($userIdentity)) {
                        $user = User::getUserByIdentity($userIdentity);
                        $session->set('uid', $user->getId());
                        $response = new RedirectResponse('/admin');
                        $response->send();
                    } else {
                        throw new Exception(null);
                    }
                } catch (\Exception $e) {
                    $session->setFlash('error', 'Account with this login/password not found. Try again.');
                    $response = new RedirectResponse('/admin/login');
                    $response->send();
                }
            } else {
                $response = new RedirectResponse('/admin/login');
                $response->send();
            }
        }
    }

    public function logout()
    {
        $user = $this->app->getComponent('user');
        if ($user->isGuest()) {
            $response = new RedirectResponse('/');
            $response->send();
        } else {
            /**
             * @var ISession $session
             */
            $session = $this->app->getComponent('session');
            $session->destroy();
            $response = new RedirectResponse('/');
            $response->send();
        }
    }

    public function edit(int $id)
    {
        $session = $this->app->getComponent('session');
        $request = $this->app->getComponent('request');
        $editForm = $request->getPost('Post');
        /**
         * @var Post $post
         */
        if ($id === 0) {
            $post = new Post();
        } else {
            $post = Post::model()->with('author')->find($id);
        }
        if (!empty($editForm)) {
            $title = $editForm['title'] ?? null;
            $text = $editForm['text'] ?? null;
            if (!empty($title)) {
                $post->title = trim($title);
            }
            if (!empty($text)) {
                $post->text = trim($text);
            }
            /**
             * Author's info
             */
            $authorForm = $editForm['author'];
            if (!empty($authorForm)) {
                if (empty($post->author_id)) {
                    $author = new Author();
                } else {
                    $author = &$post->author;
                }
                if (!empty($authorForm['firstname'])) {
                    $author->firstname = trim($authorForm['firstname']);
                }
                if (!empty($authorForm['lastname'])) {
                    $author->lastname = trim($authorForm['lastname']);
                }
                if ($author->save()) {
                    $post->author_id = $author->getLastId();
                }
            }
            if ($post->save()) {
                $session->setFlash('success', 'Article successfully saved!');
            } else {
                $session->setFlash('errors', $session->hasFlash('errors')
                    ? array_merge($session->getFlash('errors'), ['Article not saved!'])
                    : ['Article not saved!']);
            }
        }
        if ($post->isNewRecord() && $post->wasSaved()) {
            $response = new RedirectResponse('/admin/edit-post/' . $post->getLastId());
        } elseif ($post->isNewRecord() && !$post->wasSaved()) {
            $response = new RedirectResponse('/admin/new-post');
        } else {
            $response = new Response(
                $this->view->render('default/edit', [
                    'post' => $post
                ])
            );
        }
        $response->send();
    }

    public function newPost()
    {
        $post = new Post();
        $response = new Response(
            $this->view->render('default/edit', [
                'post' => $post
            ])
        );
        $response->send();
    }
}
