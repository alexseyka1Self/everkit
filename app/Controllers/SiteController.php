<?php

namespace Everkit\Base\Controllers;

use Everkit\Framework\Classes\Abstracts\ControllerAbstract;
use Everkit\Framework\Classes\CTwig;
use Everkit\Base\Models\Author;
use Everkit\Base\Models\Post;

/**
 * Class SiteController
 * @package App\Controllers
 * @var CTwig $this->view
 */
class SiteController extends ControllerAbstract
{
    public function index()
    {
        $user = $this->app->getComponent('user');
        echo $this->view->render('site/index', [
            'components' => $this->app->config->components,
            'user' => $user ?? null
        ]);
    }

    public function posts()
    {
        $posts = Post::model()->with('author')->findAll();
        echo $this->view->render('site/posts', ['posts' => $posts]);
    }

    public function postView($id)
    {
        $post = Post::model()->with(['author'])->find($id);
        echo $this->view->render('site/postView', ['post' => $post]);
    }

    public function authorView($id)
    {
        $author = Author::model()->with(['posts'])->find($id);
        echo $this->view->render('site/authorView', ['author' => $author]);
    }
}