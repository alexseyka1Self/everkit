<?php
declare(strict_types=1);

namespace Everkit\Base;

use Everkit\Framework\Classes\Abstracts\ModuleAbstract;

/**
 * Base module (root) for all application. Its required for HMVC architecture.
 * @package App
 */
class BaseModule extends ModuleAbstract
{
}