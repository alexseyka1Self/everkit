# Everkit Framework #

![Everkit Framework](https://i.imgur.com/6XxvEgu.png)

### What is this repository for? ###

* Everkit - its a HMVC framework for simple development your application. You can easily and quickly create both a prototype and a large application. Built-in ORM will help you easily write or read data from a database. Also there is a Generator System for quickly creating models, controllers, views, etc. Even console commands :)
* Version 1.43 Beta

### How do I get set up? ###

* Clone branch 'master' from our respository
* Install composer if not already installed [Install composer](https://getcomposer.org/)
* Go to the local folder with the repository
* Run 'composer install' from your terminal (or cmd.exe) to install all project dependencies
* Set-up database connection in **PROJECT_DIR/app/Config/console.php**
* Then run 'php console.php', type 'migrate' and then 'up' to create your first tables and insert sample data
* Thats all. Enjoy :)

### Who do I talk to? ###

* alexseyka1@gmail.com