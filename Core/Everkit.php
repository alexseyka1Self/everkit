<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 27.04.17
 * Time: 11:57
 */

namespace Everkit\Framework;

use Everkit\Framework\Classes\CConfig;
use Everkit\Framework\Interfaces\IComponent;
use Everkit\Framework\Interfaces\IContainer;

/**
 * Class Everkit is represented by anti-pattern Singleton
 * its keeps a self instance in $instance
 * @package App
 */
class Everkit implements IContainer
{
    /**
     * @var self
     */
    private static $instance;
    private static $registeredComponents = [];
    private static $notAllowedKeys = [];
    private static $bootstrapedComponents = [];

    private function __construct()
    {
        // Here nothing needed
    }

    protected function __clone()
    {
        // Here nothing needed
    }

    public static function app()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __get(string $key)
    {
        if (!isset(self::$registeredComponents[$key]) || in_array($key, self::$notAllowedKeys, true)) {
            throw new \InvalidArgumentException('Invalid key given');
        }

        return self::$registeredComponents[$key];
    }

    public function getComponent(string $key)
    {
        if (!isset(self::$registeredComponents[$key]) || empty(self::$registeredComponents[$key])) {
            throw new \RuntimeException(sprintf('Component %s not registered', $key));
        }

        return self::$registeredComponents[$key];
    }

    public function __debugInfo()
    {
        return self::$registeredComponents;
    }

    public function __set(string $key, $value)
    {
        if (in_array($key, self::$notAllowedKeys, true)) {
            throw new \InvalidArgumentException('Invalid key given');
        }

        self::$registeredComponents[$key] = $value;
    }

    public function setup(string $key, $value)
    {
        $this->__set($key, $value);
    }

    public function removeComponent(string $key)
    {
        if (in_array($key, self::$registeredComponents, true)) {
            unset(self::$registeredComponents[$key]);
        } else {
            throw new \RuntimeException(sprintf('Component %s not registered', $key));
        }
    }

    public function setConfig(array $config)
    {
        if (empty($config)) {
            throw new \InvalidArgumentException('Invalid key given');
        }
        if (!is_array($config)) {
            throw new \InvalidArgumentException('Config not an array');
        }
        $_config = CConfig::getInstance($config);
        $this->setup('config', $_config);
    }

    public function run()
    {
        $config = $this->getComponent('config');
        $_components = $config->components;
        if (!empty($_components)) {
            $this->bootstrappingComponents($_components);
        }
    }

    /**
     * @param array $_components
     */
    private function bootstrappingComponents(array $_components)
    {
        foreach ($_components as $key => $component) {
            if (!empty($component['class'])) {
                $instance = new $component['class'];
                $this->passParams($component, $instance);
                $this->bootstrapComponent($instance);
            }
        }
        $this->runComponents();
    }

    private function bootstrapComponent(IComponent $component)
    {
        $return = false;
        $component->setApp($this);
        if ($component->bootstrap()) {
            self::$bootstrapedComponents[] = $component;
            $return = true;
        }
        return $return;
    }

    private function runComponents()
    {
        $return = false;
        if (!empty(self::$bootstrapedComponents)) {
            foreach (self::$bootstrapedComponents as $bootstrapedComponent) {
                if ($bootstrapedComponent->run()) {
                    $return = true;
                }
            }
        }
        return $return;
    }

    private function passParams(array $component, &$instance)
    {
        $_componentParams = array_diff_key($component, ['class' => null]);
        if (!empty($_componentParams)) {
            foreach ($_componentParams as $param => $value) {
                if (property_exists($instance, $param)) {
                    $instance->{$param} = $value;
                }
            }
        }
    }
}
