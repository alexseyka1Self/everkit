<?php

/**
 * Created by Everkit Generator.
 * Date: {{ DATE }}
 * Time: {{ TIME }}
 */

namespace {{ MODULE_NAMESPACE }};

use App\Core\Classes\ControllerAbstract;

class {{ CONTROLLER_NAME }} extends ControllerAbstract
{
    public function index()
    {
        echo $this->view->render('default/index', [
            'test' => "Hello, {{ CONTROLLER_NAME }}!"
        ]);
    }
}