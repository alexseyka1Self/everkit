<?php

/**
 * Created by Everkit Generator.
 * Date: {{ DATE }}
 * Time: {{ TIME }}
 */

namespace App\Commands;

use App\Core\Classes\CCommand;

class {{ COMMAND_NAME }} extends CCommand
{

    public static $description = "{{ DESCRIPTION }}";

    public function runCommand(array $args = array()): bool
    {
        echo "Hello, {{ COMMAND_NAME }}!" . PHP_EOL;
        return true;
    }

}