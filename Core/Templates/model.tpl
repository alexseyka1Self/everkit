<?php

/**
 * Created by Everkit Generator.
 * Date: {{ DATE }}
 * Time: {{ TIME }}
 */

namespace {{ MODEL_NAMESPACE}};

use App\Core\Classes\Db\CActiveModel;

/**
 * Class Migration
 * @package App\Core\Models
 * @property integer $id
 */
class Migration extends CActiveModel
{
    public $id;

    /** @var string For ActiveModels **/
    // protected static $_tableName = "TABLE";
    // protected $_pk = 'id';
}