<?php

/**
 * Created by Everkit Generator.
 * Date: {{ DATE }}
 * Time: {{ TIME }}
 */

ini_set("display_errors", 1);
error_reporting(E_ALL);

return [
    'app_name' => 'Everkit Application',
    'version' => 1.0,
    'components' => [
        'Db' => [
            'class' => \App\Core\Classes\Db\CDataBase::class,
            'dsn' => \App\Core\Classes\Db\PdoDrivers::PDO_MYSQL . ':host=localhost;dbname=Everkit;charset=utf8',
            'user' => 'root',
            'password' => '',
        ],
        /** For web-config **/
        'RouterHandler' => [
            'routes' => [
                /**      YOUR_CONTROLLER::class . '@YOUR_ACTION' **/
                // '/' => SiteController::class . '@index',
            ]
        ],
        /** Uncomment for CLI-config **/
        // 'ErrorHandler' => null,
        // 'RequestHandler' => null,
        // 'Template' => null,
        // 'RouterHandler' => null,
        // 'CommandsHandler' => [
        //     'class' => \App\Core\Classes\CCommandsHandler::class,
        // ]
    ],
    /**
        Use $this->di->config->params from your controllers for access to params
        or use Everkit::app()->config->params from any other part of your application
    **/
    'params' => []
];