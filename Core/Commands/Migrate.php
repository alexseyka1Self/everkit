<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 04.09.17
 * Time: 14:12
 */

namespace Everkit\Framework\Commands;

use Everkit\Framework\Classes\CCommand;
use Everkit\Framework\Classes\Db\CActiveModel;
use Everkit\Framework\Classes\Db\CMigration;
use Everkit\Framework\Classes\Db\CModel;
use Everkit\Framework\Models\Migration;
use Everkit\Framework\Everkit;
use cli\Colors;
use cli\Table;
use cli\table\Ascii;

class Migrate extends CCommand
{
    public static $description = 'Migration system';
    const DATE_FORMAT = 'Ymd';
    const TIME_FORMAT = 'His';
    const NAMESPACE_TPL = '{{ NAMESPACE }}';
    const CLASS_NAME_TPL = '{{ CLASS_NAME }}';
    const OPEN_FILE_MODE = 'w';
    const DATE_TIME_FORMAT = 'Y-m-d H:i:s';
    /**
     * @var CModel $db
     */
    private $db;
    private $availableCommands;
    private $migrationTableName = 'migrations';
    private $migrationDir = APP_PATH . 'app/Migrations';
    private $migrationNamespace = "\\App\\Migrations";
    private $migrationTemplatePath = APP_PATH . 'app/Core/Templates/migration.tpl';
    private $baseMigrationName = 'm00000000_000000_base';

    public function __construct()
    {
        parent::__construct();
        $this->availableCommands = [
            ['create', 'Creates a new migration.'],
            ['history', 'Displays the migration history.'],
            ['new', 'Displays the un-applied new migrations.'],
            ['up', 'Upgrades the application by applying new migrations.'],
            ['down', 'Downgrades the application by reverting old migrations.'],
            ['clear', 'Downgrades the application to first (base) migration.']
        ];
    }

    public function runCommand(array $args = array()): bool
    {
        $database = Everkit::app()->getComponent('database');
        $this->db = new CModel(null, null, $database);
        $this->greetings();
        while ($this->loop) {
            $this->invitation();
            $cmdLine = trim(fgets(STDIN));
            $cmds = explode(' ', $cmdLine);
            $args = array();
            if (count($cmds)) {
                foreach ($cmds as $index => $cmd) {
                    if ($index === 0) {
                        $cmdLine = $cmd;
                    } else {
                        $args[] = trim(str_replace('"', '', $cmd));
                    }
                }
            }
            if (!$this->loop) {
                break;
            }
            if (empty($cmdLine)) {
                continue;
            }
            $foundMethod = $this->foundMethod($cmdLine);
            if (in_array($cmdLine, $this->quitCmdList, true)) {
                $this->loop = false;
            } elseif (!empty($foundMethod)) {
                $this->$foundMethod($args);
            } else {
                echo Colors::colorize(
                    "%RCommand '{$cmdLine}' not found!%n",
                    true
                ) . PHP_EOL;
            }
        }
        echo self::$description . ' closed.' . PHP_EOL;
        return true;
    }

    private function foundMethod($method)
    {
        $result = null;
        foreach ($this->availableCommands as $index => list($func, $description)) {
            if ($method === $func) {
                $result = $func;
            }
        }
        return $result;
    }

    private function greetings()
    {
        echo Colors::colorize(
            '%C' . self::$description . ' is started!%n',
            true
        ) . PHP_EOL;
        $this->availableCommands();
        $this->checkMigrationTable();
    }

    private function availableCommands()
    {
        $table = new Table();
        $table->setHeaders(['COMMAND NAME', 'DESCRIPTION']);
        $table->setRows($this->availableCommands);
        $table->setRenderer(new Ascii([10, 30]));
        $table->display();
    }

    private function create($args = array())
    {
        if (empty($args) || $args === array()) {
            echo Colors::colorize(
                '%CUse command:%n create NAME_OF_MIGRATION',
                true
            ) . PHP_EOL;
        } else {
            $datetime = new \DateTime();
            $migration = array_shift($args);
            $migrationName = 'm' . $datetime->format(self::DATE_FORMAT) . '_'
                . $datetime->format(self::TIME_FORMAT) . '_'
                . $migration;
            $prompt = \cli\prompt(
                sprintf('Do you want create migration "%s"?(yN)[no]', $migrationName),
                self::NEGATIVE_ANSWER
            );
            if (in_array(mb_strtolower($prompt), self::RIGHT_ANSWERS, true)) {
                $this->createMigrationFile($migrationName);
            }
        }
    }

    private function up($args = array())
    {
        $result = true;
        $newMigrations = $this->new(['display' => false]);
        if (!empty($newMigrations)) {
            foreach ($newMigrations as $className) {
                $className = array_shift($className);
                $class = $this->migrationNamespace . "\\" . $className;
                $migration = new $class();
                if (!empty($migration) && $migration instanceof CMigration) {
                    $migration->getDb()->beginTransaction();
                    if ($migration->up()) {
                        if (!$migration->getDb()->commit()) {
                            $result = false;
                        } else {
                            $migration = new Migration();
                            $migration->version = $className;
                            $migration->save();
                        }
                    } else {
                        $migration->getDb()->rollBack();
                        $result = false;
                    }
                }
            }
            $this->new();
        } else {
            $result = false;
        }
        return $result;
    }

    private function clear($args = array())
    {
        $migrations = Migration::model()->findAll("`version` <> '{$this->baseMigrationName}'");
        if (empty($migrations)) {
            echo Colors::colorize(
                '%GAlready up-to-date.%n',
                true
            ) . PHP_EOL;
        } else {
            $prompt = \cli\prompt('Do you want to restore all migrations?(yN)[no]', self::NEGATIVE_ANSWER);
            if (in_array(mb_strtolower($prompt), self::RIGHT_ANSWERS, true)) {
                while (!empty($migration = array_pop($migrations))) {
                    if ($migration->version !== $this->baseMigrationName) {
                        $migrationFile = $this->migrationDir . self::SLASH . $migration->version . self::DOT_PHP;
                        if (!is_file($migrationFile)) {
                            $this->deleteMigration($migration->version);
                        } else {
                            $migrationClass = $this->migrationNamespace . '\\' . $migration->version;
                            $migrationObj = new $migrationClass();
                            if (empty($migrationObj)) {
                                $this->deleteMigration($migration->version);
                            } else {
                                if (method_exists($migrationObj, 'down') && $migrationObj->down()) {
                                    $this->deleteMigration($migration->version);
                                }
                            }
                        }
                    }
                }
                $this->history();
            }
        }
    }

    private function down($args = array())
    {
        $migrations = Migration::model()->findAll("`version` <> '{$this->baseMigrationName}'");
        if (empty($migrations)) {
            echo Colors::colorize(
                '%GAlready all restored.%n',
                true
            ) . PHP_EOL;
        } else {
            $migration = array_pop($migrations);
            $prompt = \cli\prompt(
                sprintf('Do you want to restore migration "%s"?(yN)[no]', $migration->version),
                self::NEGATIVE_ANSWER
            );
            if (in_array(mb_strtolower($prompt), self::RIGHT_ANSWERS, true)) {
                if (!empty($migration) && $migration->version !== $this->baseMigrationName) {
                    $migrationFile = $this->migrationDir . self::SLASH . $migration->version . self::DOT_PHP;
                    if (!is_file($migrationFile)) {
                        $this->deleteMigration($migration->version);
                    } else {
                        $migrationClass = $this->migrationNamespace . '\\' . $migration->version;
                        $migrationObj = new $migrationClass();
                        if (empty($migrationObj)) {
                            $this->deleteMigration($migration->version);
                        } else {
                            if (method_exists($migrationObj, 'down') && $migrationObj->down()) {
                                $this->deleteMigration($migration->version);
                            }
                        }
                    }
                }
                $this->history();
            }
        }
    }

    private function deleteMigration($version = null)
    {
        if (!empty($version)) {
            /**
             * @var CActiveModel $_migration
             */
            $_migration = Migration::model()->find(
                sprintf("version='%s'", $version)
            );
            $_migration->delete();
        }
    }

    private function history()
    {
        $migrations = Migration::model()->findAll();
        $data = array();
        foreach ($migrations as $index => $migration) {
            $data[$index][] = $migration->version;
            $data[$index][] = $migration->apply_time;
        }
        $table = new Table();
        $table->setHeaders(['MIGRATION NAME', 'APPLY_TIME']);
        $table->setRows($data);
        $table->setRenderer(new Ascii([30]));
        $table->display();
    }

    private function new(array $args = array('display' => true))
    {
        $dirHandler = opendir($this->migrationDir);
        $filesInDir = array();
        while (false !== ($entry = readdir($dirHandler))) {
            if (!in_array($entry, [self::DOT, self::DOUBLE_DOT], true)) {
                $filesInDir[] = $entry;
            }
        }
        asort($filesInDir);
        if (!empty($filesInDir) && $filesInDir !== array()) {
            foreach ($filesInDir as $index => $file) {
                $migrationName = rtrim($file, self::DOT_PHP);
                if (null !== $this->db && $this->db instanceof CModel) {
                    $exists = Migration::model()->find(
                        sprintf("version='%s'", $migrationName)
                    );
                    if (!empty($exists)) {
                        unset($filesInDir[$index]);
                    } else {
                        $filesInDir[$index] = [$migrationName];
                    }
                }
            }
        }
        if (!empty($filesInDir) && $filesInDir !== array()) {
            if (empty($args['display']) || $args['display'] !== false) {
                $table = new Table();
                $table->setHeaders(['MIGRATION NAME']);
                $table->setRows($filesInDir);
                $table->setRenderer(new Ascii([30]));
                $table->display();
            }
            $result = $filesInDir;
        } else {
            if (empty($args['display']) || $args['display'] !== false) {
                echo Colors::colorize(
                    '%GAlready up-to-date.%n',
                    true
                ) . PHP_EOL;
            }
            $result = null;
        }
        return $result;
    }

    private function createMigrationFile($filename = null)
    {
        if (!empty($filename)) {
            $filename .= self::DOT_PHP;
            $tmpName = $filename;
            $filename = $this->migrationDir . self::SLASH . $filename;
            $namespace = trim(str_replace(APP_PATH, null, $filename), self::SLASH);
            $namespace = str_replace([self::TINY_APP, self::SLASH, $tmpName], ["\\App", "\\", null], $namespace);
            $namespace = trim(trim($namespace, "\\"));

            $dir = rtrim(str_replace($tmpName, null, $filename), self::SLASH);
            try {
                if (!@mkdir($dir) && !is_dir($dir)) {
                    throw new \Exception('Could not create directory ' . $dir);
                }
                $template = file_get_contents($this->migrationTemplatePath);
                $template = str_replace(
                    [self::NAMESPACE_TPL, self::CLASS_NAME_TPL],
                    [$namespace, trim($tmpName, self::DOT_PHP)],
                    $template
                );
                $file = fopen($filename, self::OPEN_FILE_MODE);
                fwrite($file, $template);
                fflush($file);
                fclose($file);
                echo Colors::colorize(
                    '%G' . $filename . ' has been created!%n',
                    true
                ) . PHP_EOL;
                $this->new();
            } catch (\Exception $e) {
                echo Colors::colorize(
                    '%R' . $e->getMessage() . '%n',
                    true
                ) . PHP_EOL;
            }
        }
    }

    private function checkMigrationTable()
    {
        $tables = array();
        $tablesList = $this->db->showTables();
        if (!empty($tablesList)) {
            foreach ($tablesList as $list) {
                if (is_array($list)) {
                    foreach ($list as $item) {
                        $tables[] = $item;
                    }
                } else {
                    $tables[] = $list;
                }
            }
        }
        if (!in_array($this->migrationTableName, $tables, true)) {
            $this->createMigrationTable();
        }
    }

    private function createMigrationTable()
    {
        $this->db->beginTransaction();
        $query = sprintf('CREATE TABLE `%s` (
          `version` varchar(180) NOT NULL,
          `apply_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (`version`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;', $this->migrationTableName);
        $this->db->createCommand($query)->execute();
        $query = sprintf(
            'INSERT INTO `%s` (version) VALUES (:version)',
            $this->migrationTableName
        );
        $this->db->createCommand($query)->bindValue(':version', $this->baseMigrationName)->execute();
        if (!$this->db->commit()) {
            $this->db->rollBack();
            return false;
        }
        return true;
    }

    private function invitation()
    {
        $date = new \DateTime();
        $invite = '[' . $date->format(self::DATE_TIME_FORMAT) . ']['
            . Colors::colorize(
                '%Cmigrate%n',
                true
            ) . ']> ';
        fwrite(STDOUT, $invite);
    }
}
