<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 07.09.17
 * Time: 11:00
 */
declare(strict_types=1);
namespace Everkit\Framework\Commands;

use Everkit\Framework\Classes\CCommand;
use Everkit\Framework\Classes\Db\CModel;
use Everkit\Framework\Everkit;
use cli\Colors;
use cli\Table;
use cli\table\Ascii;
use cli\Tree;
use cli\tree\Markdown;

class Generator extends CCommand
{
    public static $description = 'Generator system (modules, controllers, etc.)';
    const DATE_TPL = '{{ DATE }}';
    const TIME_TPL = '{{ TIME }}';
    const MODULE_NAME_TPL = '{{ MODULE_NAME }}';
    const MODULE_NAMESPACE_TPL = '{{ MODULE_NAMESPACE }}';
    const SLASH_CONTROLLERS = '/Controllers';
    const SLASH_VIEWS = '/Views';
    const MODULE_BASE_NAME = 'Module';
    const CONTROLLER_NAME_TPL = '{{ CONTROLLER_NAME }}';
    const OPEN_FILE_MODE = 'x';
    const NO_PROMPT = 'no-prompt';
    const DATE_FORMAT = 'Y-m-d';
    const TIME_FORMAT = 'H:i:s';
    const COMMAND_NAME_TPL = '{{ COMMAND_NAME }}';
    const DESCRIPTION_TPL = '{{ DESCRIPTION }}';
    const COMMAND_BASE_NAME = 'Simple command';
    /**
     * @var CModel $db
     */
    private $db;
    private $availableCommands;
    private $mainComponents;
    private $templatesPath = APP_PATH . 'Core/Templates/';
    private $configsPath = APP_PATH . 'app/Config/';
    private $modulesNamespace = "App\\Modules";
    private $modulesPath = 'app/Modules/';
    private $defaultControllerName = 'DefaultController.php';
    private $defaultControllerBaseName = 'Default';
    private $defaultViewName = 'index';

    public function __construct()
    {
        parent::__construct();
        $this->mainComponents = ['modules', 'controllers', 'models', 'views', 'commands'];
        $this->availableCommands = [
            ['config', 'Creates a new config file.'],
            ['module', 'Creates a new module file structure.'],
            ['model', 'Creates a new model.'],
            ['controller', 'Creates a new controller.'],
            ['view', 'Creates a new view file.'],
            ['command', 'Creates a new CLI command.']
        ];
    }

    public function runCommand(array $args = array()): bool
    {
        $database = Everkit::app()->getComponent('database');
        $this->db = new CModel(null, null, $database);
        $this->greetings();
        while ($this->loop) {
            $this->invitation();
            $cmdLine = trim(fgets(STDIN));
            $cmds = explode(' ', $cmdLine);
            $args = array();
            if (count($cmds)) {
                foreach ($cmds as $index => $cmd) {
                    if ($index === 0) {
                        $cmdLine = $cmd;
                    } else {
                        $args[] = trim(str_replace('"', '', $cmd));
                    }
                }
            }
            if (!$this->loop) {
                break;
            }
            if (empty($cmdLine)) {
                continue;
            }
            $foundMethod = $this->foundMethod($cmdLine);
            if (in_array($cmdLine, $this->quitCmdList, true)) {
                $this->loop = false;
            } elseif (!empty($foundMethod)) {
                $this->$foundMethod($args);
            } else {
                echo Colors::colorize(
                    "%RCommand '{$cmdLine}' not found!%n",
                    true
                ) . PHP_EOL;
            }
        }
        echo self::$description . ' closed.' . PHP_EOL;
        return true;
    }

    private function config(array $args = array())
    {
        $result = false;
        if (!empty($args) && $args !== array() && count($args)) {
            $configName = mb_strtolower(trim(array_shift($args))) . self::DOT_PHP;
            $prompt = \cli\prompt(
                sprintf('Do you want create config "%s"?(yN)[no]', $configName),
                self::NEGATIVE_ANSWER
            );
            if (in_array(mb_strtolower($prompt), self::RIGHT_ANSWERS, true)) {
                if (is_file($this->configsPath . $configName)) {
                    printf('Config %s already exists.' . PHP_EOL, $configName);
                } else {
                    $templateContent = file_get_contents($this->templatesPath . 'config.tpl');
                    if (!empty($templateContent)) {
                        $datetime = new \DateTime();
                        $templateContent = str_replace(
                            [self::DATE_TPL, self::TIME_TPL],
                            [$datetime->format(self::DATE_FORMAT), $datetime->format(self::TIME_FORMAT)],
                            $templateContent
                        );
                        $fullConfigPath = $this->configsPath . $configName;
                        if (false !== ($fileHandler = @fopen($fullConfigPath, self::OPEN_FILE_MODE))) {
                            if (fwrite($fileHandler, $templateContent)) {
                                echo Colors::colorize(
                                    '%G'
                                        .sprintf('New config successfully created in %s', $fullConfigPath). '!%n',
                                    true
                                ) . PHP_EOL;
                                $result = true;
                            }
                            fflush($fileHandler);
                            fclose($fileHandler);
                        } else {
                            echo Colors::colorize(
                                '%RConfig already exists!%n',
                                true
                            ) . PHP_EOL;
                        }
                    }
                }
            }
        } else {
            echo Colors::colorize(
                '%CUse command:%n config NAME_OF_NEW_CONFIG',
                true
            ) . PHP_EOL;
        }
        return $result;
    }

    private function module(array $args = array())
    {
        $result = false;
        if (!empty($args) && $args !== array() && count($args)) {
            $moduleBaseName = ucfirst(mb_strtolower(trim(array_shift($args))));
            if ($this->createModuleStructure($moduleBaseName)) {
                // COPY MODULE FILE
                $template = file_get_contents($this->templatesPath . 'module.tpl');
                $moduleNamespace = $this->modulesNamespace . "\\" . ucfirst($moduleBaseName);
                $datetime = new \DateTime();
                $date = $datetime->format(self::DATE_FORMAT);
                $time = $datetime->format(self::TIME_FORMAT);
                $modulePath = str_replace([self::BIG_APP, "\\"], [self::TINY_APP, self::SLASH], $moduleNamespace);
                $template = str_replace(
                    [self::MODULE_NAME_TPL, self::MODULE_NAMESPACE_TPL, self::DATE_TPL, self::TIME_TPL],
                    [$moduleBaseName . self::MODULE_BASE_NAME, $moduleNamespace, $date, $time],
                    $template
                );

                if (false !== ($fileHandler = @fopen(
                    $this->modulesPath . ucfirst($moduleBaseName) . self::SLASH
                        . ucfirst($moduleBaseName . self::MODULE_BASE_NAME) . self::DOT_PHP,
                    self::OPEN_FILE_MODE
                ))) {
                    $createModule = fwrite($fileHandler, $template);
                    $createController = $this->controller([
                        $this->defaultControllerBaseName,
                        $modulePath . self::SLASH_CONTROLLERS,
                        self::NO_PROMPT => true
                    ]);
                    $createView = $this->view([
                        $this->defaultViewName,
                        $modulePath . '/Views/' . mb_strtolower($this->defaultControllerBaseName),
                        self::NO_PROMPT => true
                    ]);
                    if ($createModule && $createController && $createView) {
                        $result = true;
                        $fileStructure = [
                            'Controllers' => [$this->defaultControllerName],
                            'Views' => [
                                $modulePath . self::SLASH_VIEWS => [
                                    mb_strtolower($this->defaultControllerBaseName) => ['index.html.twig']
                                ]
                            ],
                            ucfirst($moduleBaseName . self::MODULE_BASE_NAME) . self::DOT_PHP
                        ];
                        echo Colors::colorize(
                            '%GNew module successfully created with next files structure :%n',
                            true
                        ) . PHP_EOL;
                        $tree = new Tree;
                        $tree->setData($fileStructure);
                        $tree->setRenderer(new Markdown(2));
                        $tree->display();
                    }
                    fflush($fileHandler);
                    fclose($fileHandler);
                } else {
                    echo Colors::colorize(
                        '%RModule already exists!%n',
                        true
                    ) . PHP_EOL;
                }
            }
        } else {
            echo Colors::colorize(
                '%CUse command:%n module <NAME_OF_NEW_MODULE>',
                true
            ) . PHP_EOL;
        }
        return $result;
    }

    private function createModuleStructure($moduleBaseName = null)
    {
        $result = false;
        if (!empty($moduleBaseName)) {
            $moduleName = $moduleBaseName . self::MODULE_BASE_NAME;
            $prompt = \cli\prompt(
                sprintf('Do you want create module "%s"?(yN)[no]', $moduleName),
                self::NEGATIVE_ANSWER
            );
            if (in_array(mb_strtolower($prompt), self::RIGHT_ANSWERS, true)) {
                $viewsDir = str_replace(
                    'controller',
                    null,
                    mb_strtolower(rtrim($this->defaultControllerName, self::DOT_PHP))
                );
                // CREATE MODULE FOLDER
                if (!@mkdir($this->modulesPath . $moduleBaseName)
                    && !is_dir($this->modulesPath . $moduleBaseName)
                ) {
                    printf('Module %s already exists!' . PHP_EOL, $moduleName);
                } else {
                    // CREATE CONTROLLERS FOLDER
                    $moduleControllersDir = $this->modulesPath . $moduleBaseName . self::SLASH_CONTROLLERS;
                    if (!@mkdir($moduleControllersDir) && !is_dir($moduleControllersDir)) {
                        printf('Cannot create folder %s.' . PHP_EOL, $moduleControllersDir);
                    } else {
                        // CREATE VIEWS FOLDER
                        $moduleViewsDir = $this->modulesPath . $moduleBaseName . self::SLASH_VIEWS;
                        if (!@mkdir($moduleViewsDir) && !is_dir($moduleViewsDir)) {
                            printf('Cannot create folder %s.' . PHP_EOL, $moduleViewsDir);
                        } else {
                            $moduleViewDir = $this->modulesPath . $moduleBaseName
                                . sprintf('/Views/%s', $viewsDir);
                            if (!@mkdir($moduleViewDir) && !is_dir($moduleViewDir)) {
                                printf('Cannot create folder %s.' . PHP_EOL, $moduleViewDir);
                            } else {
                                $result = true;
                            }
                        }
                    }
                }
            }
        }
        return $result;
    }

    private function model(array $args = array())
    {
        $result = false;
        if (!empty($args) && $args !== array() && count($args) > 1) {
            $modelName = ucfirst(mb_strtolower(trim(array_shift($args))));
            $noPrompt = !empty($args[self::NO_PROMPT]) ? $args[self::NO_PROMPT] : false;
            if (!$noPrompt) {
                $promptResult = false;
                $prompt = \cli\prompt(
                    sprintf('Do you want create model "%s"?(yN)[no]', $modelName),
                    self::NEGATIVE_ANSWER
                );
                if (in_array(mb_strtolower($prompt), self::RIGHT_ANSWERS, true)) {
                    $promptResult = true;
                }
            }
            if ($noPrompt || !empty($promptResult)) {
                $namespaceParts = explode(self::SLASH, trim(array_shift($args), self::SLASH));
                array_walk($namespaceParts, function (&$item) {
                    $item = ucfirst(mb_strtolower(trim($item)));
                });
                $path = str_replace(self::BIG_APP, self::TINY_APP, implode(self::SLASH, $namespaceParts));
                $namespace = implode("\\", $namespaceParts);
                $template = file_get_contents($this->templatesPath . 'model.tpl');
                $datetime = new \DateTime();
                $template = str_replace(
                    [self::MODULE_NAMESPACE_TPL, self::CONTROLLER_NAME_TPL, self::DATE_TPL, self::TIME_TPL],
                    [
                        $namespace,
                        $modelName,
                        $datetime->format(self::DATE_FORMAT),
                        $datetime->format(self::TIME_FORMAT)
                    ],
                    $template
                );
                $filename = APP_PATH . $path . self::SLASH . $modelName . self::DOT_PHP;
                if (false !== ($fileHandler = @fopen($filename, self::OPEN_FILE_MODE))) {
                    if (fwrite($fileHandler, $template)) {
                        $result = true;
                        if (!$noPrompt) {
                            echo Colors::colorize(
                                "%GNew model successfully created in %C{$filename}!%n",
                                true
                            ) . PHP_EOL;
                        }
                        fflush($fileHandler);
                        fclose($fileHandler);
                    }
                } else {
                    echo Colors::colorize(
                        '%RModel already exists!%n',
                        true
                    ) . PHP_EOL;
                }
            }
        } else {
            echo Colors::colorize(
                '%CUse command:%n controller <NAME_OF_NEW_CONTROLLER> <PATH_TO_CONTROLLERS_FOLDER>' . PHP_EOL
                    . '%CExample:%n controller Test Admin/Controllers' . PHP_EOL
                    . '%COR%n controller Test App/Controllers',
                true
            ) . PHP_EOL;
        }
        return $result;
    }

    private function controller(array $args = array())
    {
        $result = false;
        if (!empty($args) && $args !== array() && count($args) > 1) {
            $controllerName = ucfirst(mb_strtolower(trim(array_shift($args)))) . 'Controller';
            $noPrompt = !empty($args[self::NO_PROMPT]) ? $args[self::NO_PROMPT] : false;
            if (!$noPrompt) {
                $promptResult = false;
                $prompt = \cli\prompt(
                    sprintf('Do you want create controller "%s"?(yN)[no]', $controllerName),
                    self::NEGATIVE_ANSWER
                );
                if (in_array(mb_strtolower($prompt), self::RIGHT_ANSWERS, true)) {
                    $promptResult = true;
                }
            }
            if ($noPrompt || !empty($promptResult)) {
                $namespaceParts = explode(self::SLASH, trim(array_shift($args), self::SLASH));
                array_walk($namespaceParts, function (&$item) {
                    $item = ucfirst(mb_strtolower(trim($item)));
                });
                $path = str_replace(self::BIG_APP, self::TINY_APP, implode(self::SLASH, $namespaceParts));
                $namespace = implode("\\", $namespaceParts);
                $template = file_get_contents($this->templatesPath . 'controller.tpl');
                $datetime = new \DateTime();
                $template = str_replace(
                    [self::MODULE_NAMESPACE_TPL, self::CONTROLLER_NAME_TPL, self::DATE_TPL, self::TIME_TPL],
                    [
                        $namespace,
                        $controllerName,
                        $datetime->format(self::DATE_FORMAT),
                        $datetime->format(self::TIME_FORMAT)
                    ],
                    $template
                );
                $filename = APP_PATH . $path . self::SLASH . $controllerName . self::DOT_PHP;
                if (false !== ($fileHandler = @fopen($filename, self::OPEN_FILE_MODE))) {
                    if (fwrite($fileHandler, $template)) {
                        $result = true;
                        if (!$noPrompt) {
                            echo Colors::colorize(
                                "%GNew controller successfully created in {$filename}!%n",
                                true
                            ) . PHP_EOL;
                        }
                        fflush($fileHandler);
                        fclose($fileHandler);
                    }
                } else {
                    echo Colors::colorize(
                        '%RController already exists!%n',
                        true
                    ) . PHP_EOL;
                }
            }
        } else {
            echo Colors::colorize(
                '%CUse command:%n controller <NAME_OF_NEW_CONTROLLER> <PATH_TO_CONTROLLERS_FOLDER>' . PHP_EOL
                    . '%CExample:%n controller Test Admin/Controllers' . PHP_EOL
                    . '%COR%n controller Test App/Controllers',
                true
            ) . PHP_EOL;
        }
        return $result;
    }

    private function view(array $args = array())
    {
        $result = false;
        if (!empty($args) && $args !== array() && count($args) > 1) {
            $viewName = mb_strtolower(trim(array_shift($args)));
            $noPrompt = !empty($args[self::NO_PROMPT]) ? $args[self::NO_PROMPT] : false;
            if (!$noPrompt) {
                $promptResult = false;
                $prompt = \cli\prompt(
                    sprintf('Do you want create view "%s"?(yN)[no]', $viewName),
                    self::NEGATIVE_ANSWER
                );
                if (in_array(mb_strtolower($prompt), self::RIGHT_ANSWERS, true)) {
                    $promptResult = true;
                }
            }
            if ($noPrompt || !empty($promptResult)) {
                $namespaceParts = explode(self::SLASH, trim(array_shift($args), self::SLASH));
                array_walk($namespaceParts, function (&$item) {
                    if (in_array($item, $this->mainComponents, true)) {
                        $item = ucfirst(mb_strtolower(trim($item)));
                    }
                });
                $path = str_replace(self::BIG_APP, self::TINY_APP, implode(self::SLASH, $namespaceParts));
                $datetime = new \DateTime();
                $template = file_get_contents($this->templatesPath . 'view.tpl');
                $template = str_replace(
                    [self::DATE_TPL, self::TIME_TPL],
                    [$datetime->format(self::DATE_FORMAT), $datetime->format(self::TIME_FORMAT)],
                    $template
                );
                $filename = APP_PATH . $path . self::SLASH . $viewName . '.html.twig';
                if (false !== ($fileHandler = @fopen($filename, self::OPEN_FILE_MODE))) {
                    if (fwrite($fileHandler, $template)) {
                        $result = true;
                        if (!$noPrompt) {
                            echo Colors::colorize(
                                "%GNew view successfully created in {$filename}!%n",
                                true
                            ) . PHP_EOL;
                        }
                        fflush($fileHandler);
                        fclose($fileHandler);
                    }
                } else {
                    echo Colors::colorize(
                        '%RView already exists!%n',
                        true
                    ) . PHP_EOL;
                }
            }
        } else {
            echo Colors::colorize(
                '%CUse command:%n view <NAME_OF_NEW_VIEW> <PATH_TO_VIEWS_FOLDER>' . PHP_EOL
                    . '%CExample:%n view Test Admin/Controllers' . PHP_EOL
                    . '%COR%n view test app/controllers',
                true
            ) . PHP_EOL;
        }
        return $result;
    }

    private function command(array $args = array())
    {
        $result = false;
        if (!empty($args) && $args !== array()) {
            $commandName = ucfirst(mb_strtolower(trim(array_shift($args))));
            $noPrompt = !empty($args[self::NO_PROMPT]) ? $args[self::NO_PROMPT] : false;
            if (!$noPrompt) {
                $promptResult = false;
                $prompt = \cli\prompt(
                    sprintf('Do you want create command "%s"?(yN)[no]', $commandName),
                    self::NEGATIVE_ANSWER
                );
                if (in_array(mb_strtolower($prompt), self::RIGHT_ANSWERS, true)) {
                    $promptResult = true;
                }
            }
            if ($noPrompt || !empty($promptResult)) {
                if (count($args)) {
                    $description = ucfirst(mb_strtolower(trim(implode(' ', $args))));
                }
                $path = 'app/Commands';
                $datetime = new \DateTime();
                $template = file_get_contents($this->templatesPath . 'command.tpl');
                $template = str_replace(
                    [self::DATE_TPL, self::TIME_TPL, self::COMMAND_NAME_TPL, self::DESCRIPTION_TPL],
                    [$datetime->format(self::DATE_FORMAT), $datetime->format(self::TIME_FORMAT), $commandName,
                        !empty($description) ? $description : self::COMMAND_BASE_NAME],
                    $template
                );
                $filename = APP_PATH . $path . self::SLASH . $commandName . self::DOT_PHP;
                if (false !== ($fileHandler = @fopen($filename, self::OPEN_FILE_MODE))) {
                    if (fwrite($fileHandler, $template)) {
                        $result = true;
                        if (!$noPrompt) {
                            echo Colors::colorize(
                                "%GNew command successfully created in {$filename}!%n",
                                true
                            ) . PHP_EOL;
                        }
                        fflush($fileHandler);
                        fclose($fileHandler);
                    }
                } else {
                    echo Colors::colorize(
                        '%RCommand already exists!%n',
                        true
                    ) . PHP_EOL;
                }
            }
        } else {
            echo Colors::colorize(
                '%CUse command:%n command <NAME_OF_NEW_COMMAND> [<DESCRIPTION>]',
                true
            ) . PHP_EOL;
        }
        return $result;
    }


    private function foundMethod($method)
    {
        $result = null;
        foreach ($this->availableCommands as $index => list($func, $description)) {
            if ($method === $func) {
                $result = $func;
            }
        }
        return $result;
    }

    private function greetings()
    {
        echo self::$description . ' is started!' . PHP_EOL;
        $this->availableCommands();
    }

    private function availableCommands()
    {
        $table = new Table();
        $table->setHeaders(['COMMAND NAME', 'DESCRIPTION']);
        $table->setRows($this->availableCommands);
        $table->setRenderer(new Ascii([10, 30]));
        $table->display();
    }

    private function invitation()
    {
        $date = new \DateTime();
        $invite = '[' . $date->format('Y-m-d H:i:s') . ']['
            . Colors::colorize(
                '%Cgenerator%n',
                true
            ) . ']> ';
        fwrite(STDOUT, $invite);
    }
}
