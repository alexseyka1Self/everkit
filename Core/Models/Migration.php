<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 06.09.17
 * Time: 12:41
 */

namespace Everkit\Framework\Models;

use Everkit\Framework\Classes\Db\CActiveModel;

/**
 * Class Migration
 * @package Everkit\Framework\Models
 * @property string $version
 * @property string $apply_time
 */
class Migration extends CActiveModel
{
    public $version;
    public $apply_time;
}