<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 15.09.17
 * Time: 17:18
 */

namespace Everkit\Framework\Models;

use Everkit\Framework\Classes\Db\CActiveModel;
use Everkit\Framework\Classes\UserIdentity;

class User extends CActiveModel
{
    public $id;
    public $login;
    public $password;

    public function getId(): int
    {
        return (int)$this->id;
    }
    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return bool
     */
    public function isGuest(): bool
    {
        $result = false;
        if (null === $this->id && null === $this->login) {
            $result = true;
        }
        return $result;
    }

    public static function checkUserIdentity(UserIdentity $userIdentity)
    {
        if (null === $userIdentity->getLogin()) {
            throw new \RuntimeException("Login is empty.");
        }
        if (null === $userIdentity->getPassword()) {
            throw new \RuntimeException('Password is empty.');
        }
        /**
         * @var self $user
         */
        $user = self::model()->find([
            'login' => $userIdentity->getLogin()
        ]);
        if (empty($user)) {
            throw new \Exception('This login is not registered.');
        }
        return password_verify($userIdentity->getPassword(), $user->getPassword());
    }

    public static function getUserByIdentity(UserIdentity $userIdentity): self
    {
        $result = null;
        if (self::checkUserIdentity($userIdentity)) {
            $user = self::model()->find([
                'login' => $userIdentity->getLogin()
            ]);
            if (!empty($user)) {
                $result = $user;
            }
        }
        return $result;
    }
}
