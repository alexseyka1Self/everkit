<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 20.09.17
 * Time: 16:01
 */

namespace Everkit\Framework\Classes\Helpers;

/**
 * Class SimpleRenderer
 * @package Everkit\Framework\Classes\Helpers
 */
class SimpleRenderer
{
    /**
     * Render php-template in HTML-code.
     * @param string $template
     * @param array $data
     * @param null $context
     * @return mixed
     */
    public static function render(string $template, array $data, $context = null)
    {
        foreach ($data as $key => $value) {
            ${$key} = $value;
        }
        unset($data);
        unset($value);
        unset($key);
        require_once $template;
    }
}
