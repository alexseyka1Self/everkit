<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 01.09.17
 * Time: 17:03
 */
declare(strict_types=1);
namespace Everkit\Framework\Classes;

use Everkit\Framework\Classes\Abstracts\ModuleAbstract;
use Everkit\Framework\Interfaces\ICommand;

class CCommand extends ModuleAbstract implements ICommand
{
    const DOT_PHP = '.php';
    const RIGHT_ANSWERS = ['y', 'yes'];
    const BIG_APP = 'App';
    const TINY_APP = 'app';
    const SLASH = '/';
    const NEGATIVE_ANSWER = 'n';
    const DOT = '.';
    const DOUBLE_DOT = '..';

    protected $quitCmdList = ['quit', 'exit'];
    protected $loop = true;

    public static $description = 'Everkit Command';
    protected function beforeCommand(): bool
    {
        return true;
    }

    public function runCommand(array $args = array()): bool
    {
        if ($this->beforeCommand()) {
            $this->afterCommand();
            return true;
        }
    }

    protected function afterCommand()
    {
    }
}
