<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 05.05.17
 * Time: 13:19
 */
declare(strict_types=1);
namespace Everkit\Framework\Classes\Db;

use Everkit\Framework\Classes\Helpers\CJsonHelper;
use Everkit\Framework\Interfaces\Db\ISqlCommand;
use Everkit\Framework\Interfaces\Db\ISqlQuery;
use Everkit\Framework\Everkit;
use Doctrine\Common\Inflector\Inflector;

class CModel implements ISqlCommand, ISqlQuery
{
    protected $_table;
    /**
     * @var string $_queryString
     */
    protected $_queryString;
    /**
     * @var array $_params
     */
    protected $_params;
    /**
     * @var mixed
     */
    protected $_connection;

    public function __construct($class = null, $tableName = null, $connection = null)
    {
        if (!empty($class)) {
            $_classParts = explode('\\', $class);
            $_table = mb_strtolower(array_pop($_classParts));
            $this->_table = Inflector::pluralize($_table);
        }
        if (!empty($tableName)) {
            $this->_table = $tableName;
        }
        if (!empty($connection)) {
            $this->_connection = $connection;
        }
        $this->clearParams();
    }

    public function getTableName()
    {
        $table = null;
        if (!empty($this->_table)) {
            $table = $this->_table;
        } else {
            $_classParts = explode('\\', get_class($this));
            $_table = mb_strtolower(array_pop($_classParts));
            $table = Inflector::pluralize($_table);
        }
        return $table;
    }

    public function createCommand(string $sql)
    {
        if (!empty($sql)) {
            $this->_queryString = $sql;
        }
        return $this;
    }

    public function bindValue(string $param = null, $value = null)
    {
        if (!empty($param) && !empty($value)) {
            $this->_params[$param] = $value;
        }
        return $this;
    }

    public function bindParams(array $params)
    {
        if (!empty($params) && is_array($params)) {
            $this->_params = array_merge($this->_params, $params);
        }
        return $this;
    }

    public function getQueryString()
    {
        return $this->_queryString;
    }

    public function beginTransaction()
    {
        return $this->_connection->beginTransaction();
    }

    public function commit()
    {
        return $this->_connection->commit();
    }

    public function rollBack()
    {
        return $this->_connection->rollBack();
    }

    public function execute(array $params = array()): int
    {
        $result = 0;
        try {
            if (!empty($params) && $params !== array()) {
                $this->_params = array_merge($this->_params, $params);
            }
            if (!empty($this->_connection) && !empty($this->_queryString)) {
                $result = $this->_connection
                    ->prepare($this->_queryString)
                    ->execute($this->_params);
            }
        } catch (\Exception $e) {
            $str = sprintf(
                'Error: %s. Query was: <br>%s',
                $e->getMessage(),
                $this->getQueryString()
            );
            if (!empty($this->_params) && $this->_params !== array()) {
                $str .= sprintf(
                    '<br>With params: %s<br>',
                    json_encode($this->_params)
                );
            }
            echo $str;
        }
        $this->clearParams();
        return (int)$result;
    }

    protected function clearParams()
    {
        $this->_params = array();
        return $this;
    }

    public function query(array $params = array())
    {
        $result = array();
        if (!empty($params) && $params !== array()) {
            $this->_params = array_merge($this->_params, $params);
        }
        if (!empty($this->_connection) && !empty($this->_queryString)) {
            $preparedQuery = $this->_connection
                ->prepare($this->_queryString);
            $preparedQuery->execute($this->_params);
            $result = $preparedQuery
                ->fetch(\PDO::FETCH_ASSOC);
        }
        return $result;
    }

    public function all()
    {
        $result = array();
        if (!empty($this->_connection) && !empty($this->_queryString)) {
            try {
                $preparedQuery = $this->_connection
                    ->prepare($this->_queryString);
                $preparedQuery->execute($this->_params);
                $result = $preparedQuery
                    ->fetchAll(\PDO::FETCH_ASSOC);
            } catch (\Exception $e) {
                $str = sprintf(
                    'Error: %s. Query was: <br>%s',
                    $e->getMessage(),
                    $this->getQueryString()
                );
                if (!empty($this->_params) && $this->_params !== array()) {
                    $str .= sprintf(
                        '<br>With params: %s<br>',
                        json_encode($this->_params)
                    );
                }
                echo $str;
            }
        }
        return $result;
    }

    public function column($column = 0)
    {
        $result = array();
        if (!empty($this->_connection) && !empty($this->_queryString)) {
            $sth = $this->_connection
                ->prepare($this->_queryString);
            $sth->execute($this->_params);
            $result = $sth
                ->fetchAll(\PDO::FETCH_COLUMN, $column);
        }
        return $result;
    }

    public function scalar($column = 0)
    {
        $result = array();
        if (!empty($this->_connection) && !empty($this->_queryString)) {
            $sth = $this->_connection
                ->prepare($this->_queryString);
            $sth->execute($this->_params);
            $result = $sth
                ->fetchColumn($column);
        }
        return $result;
    }

    public function showTables()
    {
        $query = 'SHOW TABLES';
        return $this->createCommand($query)->all();
    }
}