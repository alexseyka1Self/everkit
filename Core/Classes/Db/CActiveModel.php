<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 05.05.17
 * Time: 13:21
 */
declare(strict_types=1);
namespace Everkit\Framework\Classes\Db;

use Everkit\Framework\Interfaces\Db\IActiveModel;
use Everkit\Framework\Interfaces\Db\IModel;
use Everkit\Framework\Everkit;
use Symfony\Component\Validator\Validation;

class CActiveModel extends CModel implements IModel, IActiveModel
{
    const RELATION_TYPE = 0;
    const RELATED_MODEL = 1;
    const RELATION_PROPERTIES = 2;

    const HAS_ONE = 100;
    const HAS_MANY = 101;
    const BELONGS_TO = 102;

    const BASE_PARAMETER = ':param';

    /**
     * Everkit\Framework\Everkit
     */
    protected $_app;
    /**
     * @var bool
     */
    protected $_isNewRecord = true;
    /**
     * @var string
     */
    protected static $_tableName;
    /**
     * @var array
     */
    protected $_params;
    /**
     * @var array
     */
    protected $_insertRows;
    /**
     * @var string
     */
    protected $_pk = 'id';
    /**
     * @var string
     */
    private $_model;
    /**
     * @var array $_union
     */
    private $_union;
    /**
     * @var bool
     */
    private $_getUnion = false;
    /**
     * @var integer|string
     */
    private $_lastID;
    /**
     * @var \PDO
     */
    private $_db;
    /**
     * @var bool
     */
    protected $_saved = false;

    private $_indexParameter = 0;

    public function __construct($class = null, $tableName = null)
    {
        if (!empty($class)) {
            $this->_model = $class;
        } elseif (!empty(static::class)) {
            $this->_model = static::class;
        } else {
            $this->_model = get_class($this);
        }
        $this->_app = Everkit::app();
        $this->_db = $this->_app->getComponent('database');
        $this->_connection = $this->_db->getConnection();
        $this->_params = array();
        parent::__construct($class, $tableName);
    }

    protected function __clone()
    {
    }

    public static function model()
    {
        return new self(static::class, static::$_tableName);
    }

    public function beforeFind()
    {
        $newObj = new $this->_model;
        $this->_pk = $newObj->_pk;
        return true;
    }

    /**
     * @param mixed $condition
     */
    protected function setConditions($condition = null)
    {
        $this->createCommand("SELECT * FROM `{$this->getTableName()}`");
        if (!empty($condition)) {
            if (is_array($condition)) {
                $this->setConditionByArray($condition);
            } elseif (is_string($condition) && !preg_match('/^\d+$/', $condition)) {
                $this->setConditionByString($condition);
            } elseif (is_numeric($condition) && preg_match('/^\d+$/', (string)$condition)) {
                $this->setConditionByString('`' . $this->_pk . '`=' . $condition);
            }
        }
    }

    /**
     * @param array|mixed $condition
     * @return null
     */
    public function find($condition = null)
    {
        if ($this->beforeFind()) {
            $this->setConditions($condition);
            $query = $this->query($this->_params);
            $obj = new $this->_model;
            foreach (get_object_vars($this) as $prop => $value) {
                $obj->{$prop} = $value;
            }
            if (!empty($query)) {
                $this->setProperties($query, $obj);
                if ($obj instanceof self) {
                    $obj->notNewRecord();
                }
            } else {
                $obj = null;
            }
            if (null !== $obj) {
                $this->afterFind([$obj]);
            }
            return $obj;
        }
    }

    /**
     * @param mixed $condition
     * @return array|null
     */
    public function findAll($condition = null)
    {
        if ($this->beforeFind()) {
            $results = array();
            $this->setConditions($condition);
            $query = $this->all();
            if (!empty($query)) {
                foreach ($query as $item) {
                    $obj = new $this->_model;
                    foreach (get_object_vars($this) as $prop => $value) {
                        $obj->{$prop} = $value;
                    }
                    $this->setProperties($item, $obj);
                    if ($obj instanceof self) {
                        $obj->notNewRecord();
                    }
                    $results[] = $obj;
                }
            } else {
                $results = null;
            }
            if (!empty($results)) {
                $this->afterFind($results);
            }
            return $results;
        }
    }

    public function afterFind(array $results)
    {
        if ($this->_getUnion && !empty($results)) {
            foreach ($results as $result) {
                $result->applyUnion();
            }
        }
        $this->_table = null;
        $this->_queryString = null;
        return true;
    }

    public function beforeSave(): bool
    {
        $result = true;
        $this->clearParams();
        if (array() !== $this->validators() && false === $this->checkValidators()) {
            $result = false;
        }
        return $result;
    }

    public function save()
    {
        $result = 0;
        $attributes = '';
        $inNewRecord = $this->isNewRecord();
        if ($this->beforeSave()) {
            if ($inNewRecord) {
                $this->_insertRows = array();
            }
            $this->_queryString = sprintf('INSERT INTO `%s` ', $this->getTableName());
            if (!$this->isNewRecord()) {
                $this->_queryString = "UPDATE `{$this->getTableName()}` SET ";
            }
            /**
             * Getting model properties only (without parent default properties)
             */
            $allProperties = get_object_vars($this);
            $reflection = new \ReflectionClass(self::class);
            $defaultProperties = $reflection->getDefaultProperties();
            $modelProperties = array_diff_key($allProperties, $defaultProperties);
            foreach ($modelProperties as $key => $value) {
                if (empty($value) || $key === $this->_pk || is_array($value) || is_object($value)) {
                    continue;
                }
                $parameter = self::BASE_PARAMETER . $this->_indexParameter;
                $value = str_replace(['"', "'"], null, trim($value));
                $attributes .= sprintf('%s=%s,', $key, $parameter);
                $this->_insertRows[] = $key;
                $this->_params[$parameter] = $value;
                $this->_indexParameter++;
            }
            if ($this->isNewRecord()) {
                $this->_queryString .= sprintf(
                    '(%s) VALUES (%s)',
                    implode(',', $this->_insertRows),
                    implode(',', array_keys($this->_params))
                );
            }
            if (!empty($attributes) && $attributes !== '' && !$this->isNewRecord()) {
                $attributes = trim(trim($attributes, ','));
                $this->_queryString .= $attributes;
            }
            if (!$this->isNewRecord()) {
                $this->_queryString .= sprintf(
                    ' WHERE `%s`=%s',
                    $this->_pk,
                    $this->{$this->_pk}
                );
            }

            $result = $this->createCommand($this->_queryString)->execute($this->_params);

            $this->_lastID = $this->_db->lastInsertId();

            if ($result) {
                $this->_saved = true;
                $this->afterSave();
            }
        }
        return $result;
    }

    public function getLastId()
    {
        return $this->_lastID;
    }

    public function afterSave()
    {
        $this->clearParams();
        return true;
    }

    protected function setProperties(array $arr, &$obj)
    {
        if (!empty($arr) && $arr !== array()) {
            foreach ($arr as $key => $value) {
                $obj->{$key} = $value;
            }
        }
    }

    private function setConditionByArray(array $arr)
    {
        $this->clearParams();
        if (!empty($arr)) {
            $this->_queryString .= ' WHERE ';
            $conditionString = '';
            foreach ($arr as $prop => $condition) {
                $parameter = self::BASE_PARAMETER . $this->_indexParameter;
                $this->_params[$parameter] = $condition;
                $conditionString .= sprintf(
                    ' AND `%s`=%s',
                    $prop,
                    $parameter
                );
                $this->_indexParameter++;
            }
            $conditionString = trim(trim($conditionString, ' AND'));
            if (!empty($conditionString) && $conditionString !== '') {
                $this->_queryString .= $conditionString;
            }
            $this->_queryString .= ' ';
        }
    }

    private function setConditionByString(string $str)
    {
        if (!empty($str)) {
            $this->_queryString .= sprintf(' WHERE %s ', $str);
        }
    }

    public function isNewRecord()
    {
        return $this->_isNewRecord ? true : false;
    }

    protected function notNewRecord()
    {
        $this->_isNewRecord = false;
        return true;
    }

    /**
     * @return array[]
     */
    public function relations()
    {
        return array();
    }

    public function with($table = null)
    {
        $this->_getUnion = true;
        if (!empty($table)) {
            $this->_union = (array)$table;
        }
        return $this;
    }

    protected function applyUnion()
    {
        $relations = $this->relations();
        if (empty($relations)) {
            $newObj = new $this->_model;
            if (method_exists($newObj, 'relations')) {
                $relations = $newObj->relations();
            }
        }
        if (!empty($relations)) {
            foreach ($this->_union as $item) {
                if (array_key_exists($item, $relations)) {
                    $this->{$item} = $this->__get($item);
                }
            }
        }
    }

    public function delete()
    {
        $result = 0;
        if (!empty($this->_pk) && !empty($this->{$this->_pk})) {
            $this->_queryString = "DELETE FROM `{$this->getTableName()}` WHERE `{$this->_pk}` = :id LIMIT 1";
            $result = $this->createCommand($this->_queryString)->bindValue(':id', $this->{$this->_pk})->execute();
        } else {
            $this->_queryString = "DELETE FROM `{$this->getTableName()}` WHERE ";
            $attributes = array();
            $this->_params = array();
            /**
             * Getting model properties only (without parent default properties)
             */
            $allProperties = get_object_vars($this);
            $reflection = new \ReflectionClass(self::class);
            $defaultProperties = $reflection->getDefaultProperties();
            $modelProperties = array_diff_key($allProperties, $defaultProperties);
            foreach ($modelProperties as $key => $value) {
                if (!empty($value) && $key !== $this->_pk && !is_array($value) && !is_object($value)) {
                    $keyParam = sprintf(':%s', $key);
                    $this->_params[$keyParam] = $value;
                    $attributes[] = sprintf('`%s`=%s', $key, $keyParam);
                }
            }
            $this->_queryString = sprintf($this->_queryString . '%s LIMIT 1', implode(' AND ', $attributes));

            if (!empty($this->_params) && $this->_params !== array()) {
                $result = $this->createCommand($this->_queryString)->execute($this->_params);
            }
        }
        return $result;
    }

    public function __get($name)
    {
        $result = null;
        try {
            if (empty($this->{$name})) {
                $relations = $this->relations();
                if (!empty($relations) && $relations !== array()
                    && array_key_exists($name, $relations) && is_array($relations[$name])
                ) {
                    $relation = $relations[$name];
                    $relObjName = $relation[self::RELATED_MODEL];
                    $relationParams = array();
                    foreach ($relation[self::RELATION_PROPERTIES] as $thisProp => $prop) {
                        if (!empty($this->{$thisProp})) {
                            $relationParams[$prop] = $this->{$thisProp};
                        }
                    }
                    if (!empty($relationParams) && $relationParams !== array()) {
                        switch ($relation[self::RELATION_TYPE]) {
                            case self::HAS_ONE:
                            case self::BELONGS_TO:
                                $relatedObj = (new $relObjName())->find($relationParams);
                                break;
                            case self::HAS_MANY:
                                $relatedObj = (new $relObjName())->findAll($relationParams);
                                break;
                        }
                    }
                    if (!empty($relatedObj)) {
                        $result = $relatedObj;
                    }
                }
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        return $result;
    }

    public function wasSaved(): bool
    {
        return $this->_saved;
    }

    public function columnNames(): array
    {
        return array();
    }

    /**
     * Validators system
     * @return array of validators for model properties
     */
    public function validators(): array
    {
        return array();
    }

    /**
     * For see list of all validators
     * @see https://github.com/symfony/validator/tree/master/Constraints
     * @return bool
     */
    protected function checkValidators(): bool
    {
        $result = true;
        $errors = array();
        $validatorsList = $this->validators();
        if (array() !== $validatorsList) {
            $columnNames = $this->columnNames();
            $validateChecker = Validation::createValidator();
            foreach ($validatorsList as $property => $validators) {
                if (!empty($validators) && is_array($validators) && property_exists($this, $property)) {
                    $check = $validateChecker->validate($this->{$property}, $validators);
                    if (count($check)) {
                        foreach ($check as $violation) {
                            if (array_key_exists($property, $columnNames) && count($columnNames)) {
                                $columnTitle = $columnNames[$property];
                            } else {
                                $columnTitle = ucfirst(mb_strtolower($property));
                            }
                            $propertyErrorText = str_replace(
                                'This value',
                                $columnTitle,
                                $violation->getMessage()
                            );
                            $errors[] = $propertyErrorText;
                        }
                    }
                }
            }
            if (!empty($errors) && count($errors)) {
                $session = $this->_app->getComponent('session');
                $session->setFlash('errors', $session->hasFlash('errors')
                    ? array_merge($session->getFlash('errors'), $errors)
                    : $errors);
                $result = false;
            }
        }
        return $result;
    }
}
