<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 05.05.17
 * Time: 12:39
 */

namespace Everkit\Framework\Classes\Db;

class PdoDrivers
{
    const PDO_CUBRID = 'cubrid';
    const PDO_DBLIB = 'dblib';
    const PDO_FIREBIRD = 'firebird';
    const PDO_IBM = 'ibm';
    const PDO_INFORMIX = 'informix';
    const PDO_MYSQL = 'mysql';
    const PDO_MSSQL = 'sqlsrv';
    const PDO_ORACLE = 'oci';
    const PDO_ODBC = 'odbc';
    const PDO_SQLITE = 'sqlite';
    const PDO_PGSQL = 'pgsql';
    const PDO_4D = '4D';
}