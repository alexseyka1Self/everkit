<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 04.05.17
 * Time: 17:13
 */
declare(strict_types=1);
namespace Everkit\Framework\Classes\Db;

use Everkit\Framework\Classes\Abstracts\ComponentAbstract;
use Everkit\Framework\Everkit;

class CDataBase extends ComponentAbstract
{

    /**
     * @var string $dsn
     */
    public $dsn;
    /**
     * @var string $user
     */
    public $user;
    /**
     * @var string $password
     */
    public $password;
    /**
     * @var string $encoding
     */
    public $encoding;
    /**
     * @var \PDO $instance
     */
    private $instance;
    /**
     * @var CModel $connection
     */
    private $connection;

    public function bootstrap(): bool
    {
        $this->createInstance();
        Everkit::app()->setup('database', $this);
        return parent::bootstrap();
    }

    public function run(): bool
    {
        $this->createConnection();
        Everkit::app()->setup('db', $this->connection);
        return parent::run();
    }

    protected function createInstance()
    {
        if (empty($this->dsn)) {
            throw new \PDOException(sprintf('Not set database DSN in %s', __CLASS__));
        }

        try {
            if (strpos($this->dsn, 'sqlite') !== false) {
                $this->instance = new \PDO($this->dsn);
            } else {
                $this->instance = new \PDO($this->dsn, $this->user, $this->password);
            }
            $this->instance->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\Exception $e) {
            throw new \PDOException($e->getMessage());
        }
    }

    protected function createConnection()
    {
        if (empty($this->dsn)) {
            throw new \PDOException(sprintf('Not set database DSN in %s', __CLASS__));
        }

        $this->connection = new CModel(null, null, $this->instance);
        $this->setEncoding();
    }

    protected function setEncoding()
    {
        if (null !== $this->instance
            && $this->connection instanceof CModel
            && !empty($this->encoding)
        ) {
            $query = 'SET NAMES :encoding';
            $result = $this->connection
                ->createCommand($query)
                ->bindValue(':encoding', $this->encoding)->execute();
            return $result ? true : false;
        }
    }

    public function getConnection()
    {
        try {
            return $this->instance;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function __call($name, $arguments)
    {
        try {
            return call_user_func_array([$this->instance, $name], $arguments);
        } catch (\Exception $e) {
            throw new \PDOException($e->getMessage());
        }
    }


}
