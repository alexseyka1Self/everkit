<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 04.09.17
 * Time: 13:59
 */
declare(strict_types=1);
namespace Everkit\Framework\Classes\Db;

use Everkit\Framework\Interfaces\Db\IMigration;
use Everkit\Framework\Everkit;

class CMigration implements IMigration
{
    /**
     * @var CModel $db
     */
    protected $db;
    /**
     * @var string
     */
    private $_queryString = '';

    public function __construct()
    {
        try {
            $app = Everkit::app();
            $database = $app->getComponent('database');
            if (empty($database)) {
                throw new \Exception("Not registered component 'database'.");
            }
            $model = new CModel(null, null, $database->getConnection());
            $this->db = $model;
        } catch (\Exception $e) {
            exit($e->getMessage());
        }
    }

    public function up(): bool
    {
        return true;
    }

    public function down(): bool
    {
        return true;
    }

    protected function createTable($tableName = null, array $rows = array(), $primaryKey = null)
    {
        $result = false;
        if (!empty($tableName) && !empty($rows) && $rows !== array() && array_key_exists($primaryKey, $rows)) {
            $this->_queryString = sprintf('CREATE TABLE `%s` (', $tableName);
            $rowList = array();
            foreach ($rows as $rowName => $params) {
                $rowList[] = "`{$rowName}` " . $params;
            }
            $this->_queryString .= implode(',' . PHP_EOL, $rowList);
            if (!empty($primaryKey)) {
                $this->_queryString .= sprintf(', PRIMARY KEY (`%s`)', $primaryKey);
            }
            $this->_queryString .= ') ENGINE=InnoDb DEFAULT CHARSET=utf8';
            $result = $this->getDb()->createCommand($this->_queryString)->execute();
        }
        return $result;
    }

    protected function dropTable($tableName = null)
    {
        $result = false;
        if (!empty($tableName)) {
            $this->getDb()->beginTransaction();
            $this->_queryString = sprintf('DROP TABLE `%s`;', $tableName);
            if ($this->getDb()->createCommand($this->_queryString)->execute()) {
                $this->getDb()->commit();
                $result = true;
            } else {
                $this->getDb()->rollBack();
            }
        }
        return $result;
    }

    protected function addColumn($tableName = null, $column = null, $after = null, $first = null)
    {
        $result = false;
        if (!empty($tableName) && !empty($column)) {
            $this->_queryString = sprintf('ALTER TABLE `%s` ADD COLUMN `%s`', $tableName, $column);
            if (!empty($first) || !empty($after)) {
                if (empty($after)) {
                    $this->_queryString .= ' FIRST ';
                } elseif (empty($first)) {
                    $this->_queryString .= sprintf(' AFTER `%s` ', $after);
                }
            }
            $this->getDb()->createCommand($this->_queryString)->execute();
            $result = true;
        }
        return $result;
    }

    protected function dropColumn($tableName = null, $column = null)
    {
        $result = false;
        if (!empty($tableName) && !empty($column)) {
            $this->_queryString = sprintf('ALTER TABLE `%s` DROP COLUMN `%s`', $tableName, $column);
            $this->getDb()->createCommand($this->_queryString)->execute();
            $result = true;
        }
        return $result;
    }

    protected function renameTable($tableName = null, $newName = null)
    {
        $result = false;
        if (!empty($tableName) && !empty($newName)) {
            $this->_queryString = sprintf('ALTER TABLE `%s` RENAME TO `%s`', $tableName, $newName);
            $this->getDb()->createCommand($this->_queryString)->execute();
            $result = true;
        }
        return $result;
    }

    public function getDb()
    {
        return $this->db;
    }
}