<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 27.04.17
 * Time: 12:12
 */

namespace Everkit\Framework\Classes;

use Everkit\Framework\Classes\Components\CErrorHandler;
use Everkit\Framework\Classes\Components\CRequestHandler;
use Everkit\Framework\Classes\Components\CRouterHandler;
use Everkit\Framework\Classes\Components\CSecurityManager;
use Everkit\Framework\Classes\Components\CSessionManager;
use Everkit\Framework\Classes\Components\UserManager;
use Everkit\Framework\Classes\Helpers\CJsonHelper;

class CConfig
{
    private static $_instance = null;
    private static $storedValues = [
        'app_name' => 'Everkit Framework',
        'version' => 1.0,
        'Everkit_version' => 0.1,
        'components' => [
            'Db' => [
                'class' => Db\CDataBase::class,
                'dsn' => Db\PdoDrivers::PDO_MYSQL . ':host=localhost;dbname=framework',
                'user' => 'root',
                'password' => 'root',
                'encoding' => 'utf8'
            ],
            'ErrorHandler' => [
                'class' => CErrorHandler::class
            ],
            'SessionManager' => [
                'class' => CSessionManager::class,
                'secret' => 'my_super_secret'
            ],
            'UserManager' => [
                'class' => UserManager::class,
                'userIdentitySession' => 'uid'
            ],
            'SecurityManager' => [
                'class' => CSecurityManager::class,
                'secret' => 'secret'
            ],
            'RequestHandler' => [
                'class' => CRequestHandler::class
            ],
            'Template' => [
                'class' => CTwig::class,
                'templatesDir' => APP_PATH . 'app/Views',
                'cacheDir' => APP_PATH . 'cache',
                'cacheEnabled' => false,
                'fileExtension' => '.html.twig'
            ],
            'RouterHandler' => [
                'class' => CRouterHandler::class,
                'router' => CRouter::class,
            ],
        ],
        'widgets' => [],
        'params' => []
    ];

    private function __construct(array $config)
    {
        $this->mergeConfigs($config);
    }

    protected function __clone()
    {
    }

    public static function getInstance(array $config)
    {
        if (null === self::$_instance) {
            self::$_instance = new self($config);
        }
        return self::$_instance;
    }

    public function __get(string $key)
    {
        $return = null;
        if (!empty(self::$storedValues[$key])) {
            $return = self::$storedValues[$key];
        }
        return $return;
    }

    public function __debugInfo()
    {
        return self::$storedValues;
    }

    public function __toString(): string
    {
        return (string)CJsonHelper::encode(self::$storedValues);
    }

    private function mergeConfigs(array $config)
    {
        foreach ($config as $key => $value) {
            if (empty(self::$storedValues[$key])) {
                self::$storedValues[$key] = $value;
            } else {
                if (is_array($value) && is_array(self::$storedValues[$key])) {
                    self::$storedValues[$key] = array_replace_recursive(self::$storedValues[$key], $value);
                } elseif (!is_array($value) || !is_array(self::$storedValues[$key])) {
                    self::$storedValues[$key] = $value;
                }
            }
        }
    }

    public function getWidgetClassName(string $widgetName): string
    {
        if (array_key_exists($widgetName, $this->widgets)) {
            $className = $this->widgets[$widgetName];
        }
        return $className ?? '';
    }
}