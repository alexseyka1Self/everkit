<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 15.09.17
 * Time: 17:24
 */

namespace Everkit\Framework\Classes;

use Everkit\Framework\Interfaces\IUserIdentity;

class UserIdentity implements IUserIdentity
{
    protected $login;
    protected $password;

    public function __construct(string $login, string $password)
    {
        $this->setLogin($login);
        $this->setPassword($password);
    }

    protected function setLogin(string $login)
    {
        $login = addslashes(trim($login));
        $this->login = $login;
    }

    protected function setPassword(string $password)
    {
        $this->password = $password;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
