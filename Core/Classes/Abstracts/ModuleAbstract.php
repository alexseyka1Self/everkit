<?php
declare(strict_types=1);
namespace Everkit\Framework\Classes\Abstracts;

use Everkit\Framework\Interfaces\IModule;
use Everkit\Framework\Everkit;

/**
 * Module is an independent unit that has own controllers, models and view.
 * it can also have other built-in modules (because we use HMVC-architecture).
 * @package Everkit\Framework\Classes\Abstracts
 */
abstract class ModuleAbstract implements IModule
{
    /**
     * @var Everkit
     */
    protected $app;

    /**
     * ModuleAbstract constructor.
     */
    public function __construct()
    {
        $this->app = Everkit::app();
    }

    /**
     * This method called before the control will be transferred to controller.
     * Application stops executing if its return false.
     * @param string $controller
     * @param string $action
     * @return bool
     */
    public function beforeController(string $controller, string $action): bool
    {
        return true;
    }

    /**
     * This method called after controller (and it's action) and it can store something to model (for example).
     * @param string $controller
     * @param string $action
     * @return bool
     */
    public function afterController(string $controller, string $action): bool
    {
        return true;
    }
}
