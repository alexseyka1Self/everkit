<?php
declare(strict_types=1);
namespace Everkit\Framework\Classes\Abstracts;

use Everkit\Framework\Interfaces\IDecodable;
use Everkit\Framework\Interfaces\IEncodable;

/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 27.04.17
 * Time: 12:45
 */
abstract class JsonAbstract implements IEncodable, IDecodable
{
    /**
     * Decode json-string to mixed object (can be an array)
     * @param string $json
     * @return mixed
     */
    public static function decode(string $json): mixed
    {
        return json_decode($json);
    }

    /**
     * Encode something like object or array to json-string.
     * @param mixed $obj
     * @return string
     */
    public static function encode($obj): string
    {
        return json_encode($obj);
    }

}
