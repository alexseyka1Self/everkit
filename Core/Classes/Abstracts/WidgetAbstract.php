<?php

namespace Everkit\Framework\Classes\Abstracts;

use Everkit\Framework\Interfaces\IWidget;

/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 20.09.17
 * Time: 16:47
 */
class WidgetAbstract implements IWidget
{
    /**
     * IWidget constructor.
     * @param array $params
     */
    public function __construct(array $params = array())
    {
        if (null === $params) {
            $params = array();
        }
        foreach ($params as $param => $value) {
            if (property_exists($this, $param)) {
                $this->{$param} = $value;
            }
        }
    }
    /**
     *  Render first part if widget have a structure of block
     */
    public function start()
    {
    }

    /**
     * Render all widget template
     */
    public function render()
    {
    }

    /**
     *  Render last part if widget have a structure of block
     */
    public function end()
    {
    }
}
