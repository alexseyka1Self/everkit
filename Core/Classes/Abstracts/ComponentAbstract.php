<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 27.04.17
 * Time: 13:17
 */

namespace Everkit\Framework\Classes\Abstracts;

use Everkit\Framework\Interfaces\IComponent;
use Everkit\Framework\Everkit;

/**
 * The component is part of the Everkit composite system.
 * You can create and connect your own components and use them through the Dependency Injection container
 * (here it's the \App\Core\Everkit class).
 * The components must be inherited from the \App\Core\Interfaces\IComponent interface.
 * @package App\Core\Classes\Abstracts
 */
abstract class ComponentAbstract implements IComponent
{
    /**
     * @var Everkit $app
     */
    protected $app;

    /**
     * Setting up our abstract component in DI container
     * @return bool
     */
    public function bootstrap(): bool
    {
        return true;
    }

    /**
     * @return bool
     */
    public function run(): bool
    {
        return true;
    }

    /**
     * This method just declare property app, who refers to application (DI container).
     * @param Everkit $app
     */
    public function setApp(Everkit $app)
    {
        $this->app = $app;
    }
}
