<?php
declare(strict_types=1);

namespace Everkit\Framework\Classes\Abstracts;

use Everkit\Framework\Classes\Helpers\SimpleRenderer;
use Everkit\Framework\Interfaces\IComponent;
use Everkit\Framework\Interfaces\IController;
use Everkit\Framework\Interfaces\IRenderable;
use Everkit\Framework\Everkit;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

/**
 * Class ControllerAbstract declare controller - a part of HMVC architecture.
 * Here controller can manipulates the model (changes it state) and interpret user actions.
 * @package Everkit\Framework\Classes\Abstracts
 */
abstract class ControllerAbstract implements IComponent, IController, IRenderable
{
    const DOT_PHP = '.php';
    /**
     * Refer to aur application
     * @var Everkit $app
     */
    protected $app;
    /**
     * This property refers to our View-component (in HMVC of course).
     * View must inherit IRenderable interface for render templates in HTML.
     * @var IRenderable
     */
    protected $view;
    /**
     * The relative path to controller is stored here.
     * @var string $controllerPath
     */
    protected $controllerPath;
    /**
     * The relative path to folder with view-files.
     * @var string $viewsDir
     */
    protected $viewsDir;

    /**
     * @return bool
     */
    public function bootstrap(): bool
    {
        return true;
    }

    /**
     * @return bool
     */
    public function run(): bool
    {
        return true;
    }

    /**
     * This method called before the control will be transferred to action.
     * Application stops executing if its return false.
     * @param string $controller
     * @param string $action
     * @return bool
     */
    public function beforeAction(string $controller, string $action): bool
    {
        $this->app = Everkit::app();
        $this->view = $this->app->getComponent('view');
        $this->controllerPath = APP_PATH
            . str_replace(
                "\\",
                '/',
                str_replace(['App', 'Everkit\Base'], 'app', static::class)
            );
        $controllerPathParts = explode('/', $this->controllerPath);
        array_pop($controllerPathParts);
        array_pop($controllerPathParts);
        $this->viewsDir = implode('/', $controllerPathParts) . '/Views/';
        $this->view->viewsDir = $this->viewsDir;
        return true;
    }

    /**
     * This method called after action and it can store something to model (for example).
     * @param string $controller
     * @param string $action
     * @return bool
     */
    public function afterAction(string $controller, string $action): bool
    {
        return true;
    }

    /**
     * TODO replace this method to other component
     * @param $template
     * @param array $data
     * @return mixed|void
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException
     */
    public function render(string $template, array $data = array())
    {
        $controllerParts = explode('/', $this->controllerPath);
        $controllerName = mb_strtolower(str_replace('Controller', '', array_pop($controllerParts)));
        $viewPath = $this->viewsDir . $controllerName . '/' . $template . self::DOT_PHP;
        $viewPath = preg_replace('/\/(\w+\/\.\.\/)(\w+)\//', '/$2/', $viewPath);
        if (false === file_exists($viewPath)) {
            throw new FileNotFoundException($viewPath);
        }
        SimpleRenderer::render($viewPath, $data, $this);
    }
}
