<?php
declare(strict_types=1);

namespace Everkit\Framework\Classes\Components;

use Everkit\Base\BaseModule;
use Everkit\Framework\Classes\Abstracts\ComponentAbstract;
use Everkit\Framework\Interfaces\IController;
use Everkit\Framework\Interfaces\IModule;
use Everkit\Framework\Interfaces\IServerHandler;
use Everkit\Framework\Everkit;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CRouterHandler
 * @package Everkit\Framework\Classes\Components
 */
class CRouterHandler extends ComponentAbstract
{

    /**
     *  Separates the name of controller from the name of action.
     */
    const ACTION_DELIMITER = '@';
    /**
     * Suffix for module classes. (i.e. TestModule).
     */
    const MODULES_SUFFIX = 'Module';
    /**
     *  Regexp for searching modules files.
     */
    const MODULES_FIND = '/(.*)Modules\\\(\w+)\\\/';

    /**
     * The router redirects from the entered URL to a specific controller and action.
     */
    public $router;
    /**
     * Array of routes. ("route" => "controller@action")
     * @var array $routes
     */
    public $routes;
    /**
     * Our base module.
     * @var string
     */
    public $defaultModule = BaseModule::class;
    /**
     * @var string
     */
    public $defaultMethod = 'GET|POST';
    /**
     * @var
     */
    public $basePath;

    /**
     * @var \SplStack $moduleStack
     */
    private $moduleStack;

    /**
     * Setting up our abstract component in DI container
     * @return bool
     */
    public function bootstrap(): bool
    {
        Everkit::app()->setup('router', $this);
        return parent::bootstrap();
    }

    /**
     * @return bool
     * @throws \RuntimeException
     */
    public function run(): bool
    {
        $this->moduleStack = new \SplStack();
        $_request = $this->app->getComponent('request');
        if (!$_request) {
            throw new \RuntimeException('Component RequestHandler not registered');
        }
        if (!$_request instanceof IServerHandler) {
            throw new \RuntimeException('Component RequestHandler not provide $_SERVER');
        }
        $this->parseUrl();
        return parent::run();
    }

    /**
     * Method finds the desired controller and action, and then calls methods(beforeAction->ACTION->afterAction)
     * @throws \Exception
     * @internal param $request
     */
    private function parseUrl()
    {
        $router = new $this->router;
        $router->setBasePath($this->basePath);
        $this->createUrls($router);
        $match = $router->match();

        if ($match) {
            $target = $match['target'];
            list($controller, $action) = explode(self::ACTION_DELIMITER, $target);
            if ($this->checkModules($target, $controller, $action)) {
                $instance = new $controller;
                if ($instance instanceof IController) {
                    if ($instance->beforeAction($controller, $action)) {
                        call_user_func_array([$instance, $action], $match['params']);
                    }
                    $instance->afterAction($controller, $action);
                } else {
                    throw new \RuntimeException('Class ' . get_class($instance) . ' is not a Controller');
                }
                if (null !== $this->moduleStack) {
                    foreach ($this->moduleStack->pop() as $module) {
                        if ($module instanceof IModule) {
                            $module->afterController($controller, $action);
                        } else {
                            throw new \RuntimeException('Class ' . get_class($instance) . ' is not a Module');
                        }
                    }
                }
            }
        } else {
            $view = $this->app->getComponent('view');
            $response = new Response(
                $view->render('errors/404', array()),
                Response::HTTP_NOT_FOUND
            );
            $response->send();
        }
    }

    /**
     * @param string $target
     * @param string $controller
     * @param string $action
     * @return bool
     * @throws \Exception
     */
    private function checkModules(string $target, string $controller, string $action): bool
    {
        $return = false;
        $modules = array();
        preg_replace_callback(
            self::MODULES_FIND,
            function ($matches) use (&$modules) {
                $this->getArrayOfModules($matches, $modules);
            },
            $target
        );
        array_unshift($modules, $this->defaultModule);
        if (count($modules)) {
            $this->sortModules($modules);
            if ($this->runModules($modules, $controller, $action)) {
                $return = true;
            }
        } else {
            $return = true;
        }
        return $return;
    }

    /**
     * @param array $modules
     * @param string $controller
     * @param string $action
     * @return bool
     * @throws \Exception
     */
    private function runModules(array &$modules, string $controller, string $action): bool
    {
        $return = false;
        if (!empty($modules)) {
            foreach ($modules as $module) {
                $instance = new $module;
                if ($instance instanceof IModule) {
                    if ($instance->beforeController($controller, $action)) {
                        $this->moduleStack->push($instance);
                        $return = true;
                    } else {
                        die();
                    }
                } else {
                    throw new \RuntimeException('Class ' . get_class($instance) . ' is not a Module');
                }
            }
        }
        return $return;
    }

    private function sortModules(&$modules)
    {
        usort($modules, function ($a, $b) {
            return strlen($a) > strlen($b);
        });
    }

    /**
     * @param array $matches
     * @param array $modules
     */
    private function getArrayOfModules(array $matches, array &$modules)
    {
        if (!empty($matches)) {
            foreach ($matches as $match) {
                if (strpos($match, '\\')) {
                    if (substr_count($match, '\\') > 1) {
                        $match = rtrim($match, '\\');
                        $this->addModuleName($match);
                        $match = $this->addModuleSuffix($match);
                        $modules[] = $match;
                    }
                }
            }
        }
    }

    private function addModuleName(&$module)
    {
        if (!empty($module)) {
            $_parts = explode('\\', $module);
            $_moduleName = end($_parts);
            $module .= '\\' . $_moduleName;
        }
    }

    private function addModuleSuffix($path)
    {
        $this->addModulesPrefix($path);
        return sprintf($path . '%s', self::MODULES_SUFFIX);
    }

    private function addModulesPrefix(&$path)
    {
        if (strpos($path, 'App') === 0) {
            $path = '\\' . $path;
        }
    }

    /**
     * @param CRouter $router
     */
    private function createUrls(&$router)
    {
        if (!empty($this->routes)) {
            foreach ($this->routes as $url => $route) {
                $router->map($this->defaultMethod, $url, $route, $url);
            }
        }
    }
}
