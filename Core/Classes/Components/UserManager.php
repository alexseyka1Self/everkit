<?php
declare(strict_types=1);
namespace Everkit\Framework\Classes\Components;

use Everkit\Framework\Classes\Abstracts\ComponentAbstract;
use Everkit\Framework\Interfaces\ISessionAttributes;
use Everkit\Framework\Models\User;

class UserManager extends ComponentAbstract
{
    public $userIdentitySession;
    private $user;
    /**
     * @var ISessionAttributes $session
     */
    private $session;

    public function bootstrap(): bool
    {
        $this->session = $this->app->getComponent('session');
        return parent::bootstrap();
    }

    public function run(): bool
    {
        if (null !== $this->session && $this->session instanceof ISessionAttributes) {
            if ($id = $this->session->get($this->userIdentitySession)) {
                $this->user = User::model()->find($id);
            }
        }
        if (null === $this->user) {
            $this->user = new User();
        }
        $this->app->setup('user', $this->user);
        return parent::run();
    }
}
