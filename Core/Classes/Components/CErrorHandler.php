<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 27.04.17
 * Time: 13:36
 */

namespace Everkit\Framework\Classes\Components;

use Everkit\Framework\Classes\Abstracts\ComponentAbstract;
use Whoops\Handler\PlainTextHandler;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

/**
 * Simple handler of errors, warnings, exceptions and other.
 * @package Everkit\Framework\Classes\Components
 */
class CErrorHandler extends ComponentAbstract
{
    /**
     * Setting up our abstract component in DI container
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function bootstrap(): bool
    {
        $whoops = new Run;
        if (PHP_SAPI === 'cli') {
            $whoops->pushHandler(new PlainTextHandler());
        } else {
            $whoops->pushHandler(new PrettyPageHandler);
        }
        $whoops->register();
        return parent::bootstrap();
    }
}
