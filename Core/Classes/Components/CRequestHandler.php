<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 27.04.17
 * Time: 14:42
 */

namespace Everkit\Framework\Classes\Components;

use Everkit\Framework\Classes\Abstracts\ComponentAbstract;
use Everkit\Framework\Interfaces\IPostHandler;
use Everkit\Framework\Interfaces\IRequestHandler;
use Everkit\Framework\Interfaces\IServerHandler;
use Everkit\Framework\Everkit;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CRequestHandler
 * @package Everkit\Framework\Classes\Components
 * @property IRequestHandler $handler
 */
class CRequestHandler extends ComponentAbstract implements IRequestHandler, IPostHandler, IServerHandler
{
    /**
     * It contains a request handler.
     * @var IRequestHandler $handler
     */
    private $handler;

    /**
     * Setting up our abstract component in DI container
     * @return bool
     */
    public function bootstrap(): bool
    {
        $this->handler = Request::createFromGlobals();
        Everkit::app()->setup('request', $this);
        return parent::bootstrap();
    }

    /**
     * Method returns URI-string.
     * @return mixed
     */
    public function getUri()
    {
        return $this->handler->getUri();
    }

    /**
     * Method returns hostname-string.
     * @return mixed
     */
    public function getHost()
    {
        return $this->handler->getHost();
    }

    /**
     * Returns used method (GET, POST or other).
     * @return mixed
     */
    public function getMethod()
    {
        return $this->handler->getMethod();
    }

    /**
     * Returns all query string.
     * @return mixed
     */
    public function getQueryString()
    {
        return $this->handler->getQueryString();
    }

    /**
     * Returns query string represented in array.
     * @return array
     */
    public function getQueryArray(): array
    {
        $return = array();
        $_exp = explode('&', $this->getQueryString());
        foreach ($_exp as $param) {
            $paramValue = explode('=', $param);
            if (is_array($paramValue) && count($paramValue) > 1) {
                list($key, $value) = $paramValue;
                $return[$key] = $value;
            }
        }
        return $return;
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    public function get(string $key)
    {
        $return = null;
        $_queryArray = $this->getQueryArray();
        if (isset($_queryArray[$key])) {
            $return = $_queryArray[$key];
        }
        return $return;
    }

    /**
     * Returns array of POST-parameters
     * @return array
     */
    public function post(): array
    {
        return $_POST ?? array();
    }

    /**
     * Returns POST-parameter by its key.
     * @param string $key
     * @return mixed|null
     */
    public function getPost(string $key)
    {
        $return = null;
        $_post = $this->post();
        if (isset($_post[$key])) {
            $return = $_post[$key];
        }
        return $return;
    }

    /**
     * Returns array of server-properties.
     * @return array
     */
    public function server(): array
    {
        return $_SERVER ?? array();
    }

    /**
     * Returns server-property by its key.
     * @param string $key
     * @return null|string
     */
    public function getServer(string $key)
    {
        $_server = $this->server();
        if (isset($_server[$key])) {
            $return = $_server[$key];
        }
        return $return ?? null;
    }
}
