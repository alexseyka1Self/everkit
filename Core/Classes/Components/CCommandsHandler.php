<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 01.09.17
 * Time: 14:26
 */

namespace Everkit\Framework\Classes\Components;

use Everkit\Framework\Classes\Abstracts\ComponentAbstract;
use Everkit\Framework\Classes\CCommand;
use Everkit\Framework\Everkit;
use cli\Colors;
use cli\Table;
use cli\table\Ascii;

/**
 * It is a set of useful console commands for working with migrations, users, etc.
 * It can be extended with your own commands, just create class that will be inherited from CCommand.
 * @package Everkit\Framework\Classes\Components
 */
class CCommandsHandler extends ComponentAbstract
{
    /**
     *  Its just a greetings
     */
    const WELCOME = "  ______              _  ___ _      _____                      _       
 |  ____|            | |/ (_) |    / ____|                    | |      
 | |____   _____ _ __| ' / _| |_  | |     ___  _ __  ___  ___ | | ___  
 |  __\ \ / / _ \ '__|  < | | __| | |    / _ \| '_ \/ __|/ _ \| |/ _ \ 
 | |___\ V /  __/ |  | . \| | |_  | |___| (_) | | | \__ \ (_) | |  __/ 
 |______\_/ \___|_|  |_|\_\_|\__|  \_____\___/|_| |_|___/\___/|_|\___| 
_______________________________________________________________________" . PHP_EOL;
    /**
     *  Its just a greetings
     */
    const WELCOME2 = "You can use one of the available commands:\r\n";
    /**
     * Where the CLI-program must be stopped.
     * @var bool
     */
    private $loop = true;
    /**
     *  Built-in command for clear terminal.
     */
    const CLEAR = 'clear';
    /**
     * Built-in command for showing all available commands.
     */
    const HELP = 'help';
    /**
     * OS
     */
    const LINUX = 'Linux';
    /**
     * OS
     */
    const WINDOWS = 'Windows';
    /**
     * Clears terminal screen in Windows.
     */
    const CLEAR_SCREEN = 'cls';
    /**
     * @var \SplStack
     */
    private $commandStack;
    /**
     * All available command classes are stored here.
     * @var array
     */
    private $classList = array();
    /**
     * Available command for exiting from CLI-application or command.
     * @var array
     */
    private $quitCmdList = ['quit', 'exit'];
    /**
     * @var string
     */
    private $startInvitation = '[';
    /**
     * @var string
     */
    private $endInvitation = ']> ';
    /**
     * Invitation text for this command.
     * @var string
     */
    protected $invitation = 'Everkit Console';

    /**
     * Setting up our abstract component in DI container
     * @return bool
     */
    public function bootstrap(): bool
    {
        Everkit::app()->setup('commands', $this);
        return parent::bootstrap();
    }

    /**
     * Run our command (CLI-application).
     * @return bool
     */
    public function run(): bool
    {
        $this->commandStack = new \SplStack();
        $this->greetings();
        $this->allCommands();
        while ($this->loop) {
            $this->invitation();
            $cmdLine = trim(fgets(STDIN));
            $this->commandStack->push($cmdLine);
            if ($this->checkEnd()) {
                break;
            }
            if (empty($cmdLine)) {
                continue;
            }
            if ($cmdLine === self::CLEAR) {
                $this->clearScreen();
            } elseif ($cmdLine === self::HELP) {
                $this->allCommands();
            } else {
                if (array_key_exists($cmdLine, $this->classList) && !empty($this->classList[$cmdLine])) {
                    /**
                     * @var CCommand $cmdObject
                     */
                    $cmdObject = new $this->classList[$cmdLine];
                    $tempInvitation = $this->invitation;
                    $datetime = new \DateTime();
                    $this->invitation = sprintf($datetime->format('Y-m-d H:i:s') . '][%s', $cmdLine);
                    $this->invitation();
                    $cmdObject->runCommand();
                    $this->invitation = $tempInvitation;
                } else {
                    $this->unknownCommandMessage();
                }
            }
        }
        $this->goodbye();
        return true;
    }

    /**
     * Show the message when used unknown command.
     */
    private function unknownCommandMessage()
    {
        fwrite(STDOUT, sprintf(
            'Unknown command "%s"' . PHP_EOL,
            $this->commandStack->top()
        ));
    }

    /**
     * Check application loop. And stopped application (or command) when user used one of the Quit commands.
     * @return bool
     */
    private function checkEnd(): bool
    {
        $result = false;
        $lastCmd = $this->commandStack->top();
        if (in_array($lastCmd, $this->quitCmdList, true)) {
            $result = true;
        }
        return $result;
    }

    /**
     *  Just shows greetings message.
     */
    private function greetings()
    {
        $this->clearScreen();
        fwrite(STDOUT, Colors::colorize(
            '%C%8' . self::WELCOME . '%n' . self::WELCOME2,
            true
        ));
    }

    /**
     *  Clears the screen depending on the operation system.
     */
    private function clearScreen()
    {
        if (PHP_OS === self::LINUX) {
            system(self::CLEAR);
        } elseif (PHP_OS === self::WINDOWS) {
            system(self::CLEAR_SCREEN);
        }
    }

    /**
     *  Shows invitation line.
     */
    private function invitation()
    {
        $invite = $this->startInvitation . Colors::colorize(
            '%C' . $this->invitation . '%n',
            true
        ) . $this->endInvitation;
        fwrite(STDOUT, $invite);
    }

    /**
     *  Message
     */
    private function goodbye()
    {
        $Everkit = sprintf('[Everkit v%s] ', Everkit::app()->config->Everkit_version);
        fwrite(STDOUT, $Everkit . "Good bye! See you later.\r" . PHP_EOL);
    }

    /**
     *  Display available commands.
     */
    protected function allCommands()
    {
        require_once APP_PATH . 'vendor/wp-cli/php-cli-tools/examples/common.php';
        $headers = array('COMMAND NAME', 'DESCRIPTION');
        $data = array(
            [self::HELP, 'Show list of available commands'],
            ['quit or exit', 'Leave this program'],
            [self::CLEAR, 'Clear command screen']
        );

        $commands = glob(APP_PATH . '{app/Commands/*.php}', GLOB_BRACE);
        $coreCommands = glob(APP_PATH . '{Core/Commands/*.php}', GLOB_BRACE);
        $commands = array_merge($commands, $coreCommands);

        foreach ($commands as $file) {
            require_once $file;
            $class = basename($file, '.php');
            $class = mb_strtolower($class);
            $classWithNamespace = str_replace([APP_PATH, 'app'], ['', 'App'], $file);
            $classWithNamespace = rtrim($classWithNamespace, '.php');
            $classWithNamespace = str_replace(
                ['/', 'Core', 'core'],
                ['\\', 'Everkit\Framework', 'Everkit\Framework'],
                $classWithNamespace
            );

            $cmdObject = new $classWithNamespace;
            if (!empty($cmdObject::$description)) {
                $data[] = [$class, $cmdObject::$description];
            }
            $this->classList[$class] = $classWithNamespace;
        }
        $table = new Table();
        $table->setHeaders($headers);
        $table->setRows($data);
        $table->setRenderer(new Ascii([10, 30]));
        $table->display();
    }
}
