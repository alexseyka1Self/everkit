<?php
declare(strict_types=1);
namespace Everkit\Framework\Classes\Components;

use Everkit\Framework\Classes\Abstracts\ComponentAbstract;
use Everkit\Framework\Interfaces\IFlashBag;
use Everkit\Framework\Interfaces\ISession;
use Everkit\Framework\Interfaces\ISessionAttributes;
use Everkit\Framework\Everkit;

/**
 * Class CSessionManager
 * @package Everkit\Framework\Classes\Components
 */
class CSessionManager extends ComponentAbstract implements ISession, ISessionAttributes, IFlashBag
{
    const FLASHES = 'flashes';
    /**
     * @var Everkit $app;
     */
    protected $app;
    public $target;
    public $secret;
    private $attributes;
    private $flashes = array();

    public function bootstrap(): bool
    {
        $this->attributes = array();
        $this->app->setup('session', $this);
        return parent::bootstrap();
    }

    public function run(): bool
    {
        if (!$this->isStarted()) {
            if ($this->start()) {
                $this->setDefaultName();
                if (empty($this->target)) {
                    $this->target = &$_SESSION;
                }
                $this->fromState($this->target);
            }
        }
        return parent::run();
    }

    public function start(): bool
    {
        return session_start();
    }

    public function isStarted(): bool
    {
        $result = false;
        if (session_status() !== PHP_SESSION_NONE) {
            $result = true;
        }
        return $result;
    }

    public function getSession(): array
    {
        $result = array();
        if ($this->isStarted()) {
            $result = $_SESSION;
        }
        return $result;
    }

    protected function fromState(array $state)
    {
        if (!empty($state) && $state !== array()) {
            foreach ($state as $key => $item) {
                if ($key === self::FLASHES) {
                    $this->flashes = $item;
                    unset($state[$key]);
                }
            }
            unset($this->target[self::FLASHES]);
            $this->attributes = $state;
        }
    }

    public function destroy(): bool
    {
        return session_destroy();
    }

    public function getId(): string
    {
        $result = null;
        if ($this->isStarted()) {
            $result = session_id();
        }
        return $result;
    }

    public function setId(string $id): bool
    {
        $result = false;
        if ($this->isStarted() && session_id($id)) {
            $result = true;
        }
        return $result;
    }

    public function getName(): string
    {
        $result = null;
        if ($this->isStarted()) {
            $result = session_name();
        }
        return $result;
    }

    protected function setDefaultName()
    {
        $config = $this->app->getComponent('config');
        if (null !== $config->app_name) {
            $newSessionName = md5($config->app_name);
            if (null !== $config->version) {
                $newSessionName = md5($newSessionName . $config->version);
            }
            if (null !== $this->secret) {
                $newSessionName = md5($newSessionName . $this->secret);
            }
            $this->setName($newSessionName);
        }
    }

    public function setName(string $name): bool
    {
        $result = false;
        if ($this->isStarted() && session_name($name)) {
            $result = true;
        }
        return $result;
    }

    public function has(string $name): bool
    {
        $result = false;
        if (null !== $name && array_key_exists($name, $this->attributes)) {
            $result = true;
        }
        return $result;
    }

    public function get(string $name)
    {
        $result = null;
        if (null !== $name && array_key_exists($name, $this->attributes)) {
            $result = $this->attributes[$name];
        }
        return $result;
    }

    public function set(string $name, $value)
    {
        if (null !== $name) {
            $this->target[$name] = $value;
        }
    }

    public function all(): array
    {
        return $this->attributes;
    }

    public function getFlashes(): array
    {
        return $this->flashes;
    }

    public function getFlash(string $key)
    {
        $result = null;
        if ($this->hasFlash($key)) {
            $result = $this->flashes[$key];
        }
        return $result;
    }

    public function setFlash(string $key, $value)
    {
        $this->target[self::FLASHES][$key] = $value;
        $this->flashes[$key] = $value;
    }

    public function hasFlash(string $key): bool
    {
        $result = false;
        if (array_key_exists($key, $this->flashes)) {
            $result = true;
        }
        return $result;
    }
}
