<?php
declare(strict_types=1);
namespace Everkit\Framework\Classes\Components;

use Everkit\Framework\Classes\Abstracts\ComponentAbstract;
use Everkit\Framework\Interfaces\ISessionAttributes;

/**
 * Class CSecurityManager
 * @package Everkit\Framework\Classes\Components
 */
class CSecurityManager extends ComponentAbstract
{
    const TOKEN_SEPARATOR = ':';
    /**
     * The secret string (salt for generation tokens).
     * @var string $secret
     */
    public $secret;
    /**
     * @var string $csrf_token
     */
    protected $csrf_token;

    const CSRF_TOKEN = '_csrf_token';

    /**
     * Setting up our abstract component in DI container
     * @return bool
     */
    public function bootstrap(): bool
    {
        $this->app->setup('security', $this);
        return parent::bootstrap();
    }

    /**
     * Generates a new CSRF-token
     * @return bool
     */
    public function run(): bool
    {
        $this->generateCsrfToken();
        return parent::run();
    }

    /**
     *  Generates a new token by using salt and random string.
     */
    protected function generateCsrfToken()
    {
        $salt = bin2hex(random_bytes(4));
        $this->csrf_token = $salt . self::TOKEN_SEPARATOR . md5(
            $salt . self::TOKEN_SEPARATOR . $this->secret
        );
    }

    /**
     * Getter for token.
     * @return string
     */
    public function getCsrfToken()
    {
        /**
         * @var ISessionAttributes $session
         */
        $session = $this->app->getComponent('session');
        $session->set(self::CSRF_TOKEN, $this->csrf_token);
        return $this->csrf_token;
    }

    /**
     * Method compare transmitted $token with self::CSRF_TOKEN
     * @param string $token
     * @return bool
     */
    public function checkCsrfToken(string $token): bool
    {
        /**
         * @var ISessionAttributes $session
         */
        $session = $this->app->getComponent('session');
        $result = $token === $session->get(self::CSRF_TOKEN);
        $session->set(self::CSRF_TOKEN, null);
        return $result;
    }
}
