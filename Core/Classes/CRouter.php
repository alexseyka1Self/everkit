<?php
declare(strict_types=1);
/**
 *
 *                    // Match all request URIs
[i]                  // Match an integer
[i:id]               // Match an integer as 'id'
[a:action]           // Match alphanumeric characters as 'action'
[h:key]              // Match hexadecimal characters as 'key'
[:action]            // Match anything up to the next / or end of the URI as 'action'
[create|edit:action] // Match either 'create' or 'edit' as 'action'
[*]                  // Catch all (lazy, stops at the next trailing slash)
[*:trailing]         // Catch all as 'trailing' (lazy)
[**:trailing]        // Catch all (possessive - will match the rest of the URI)
.[:format]?          // Match an optional parameter 'format' - a / or . before the block is also optional

'i'  => '[0-9]++'
'a'  => '[0-9A-Za-z]++'
'h'  => '[0-9A-Fa-f]++'
'*'  => '.+?'
'**' => '.++'
''   => '[^/\.]++'
 */


namespace Everkit\Framework\Classes;

use Everkit\Framework\Everkit;
use Traversable;

class CRouter
{
    /**
     * @var array Array of all routes (incl. named routes).
     */
    protected $routes = array();

    /**
     * @var array Array of all named routes.
     */
    protected $namedRoutes = array();

    /**
     * @var string Can be used to ignore leading part of the Request URL (if main file lives in subdirectory of host)
     */
    protected $basePath = '';

    /**
     * @var array Array of default match types (regex helpers)
     */
    protected $matchTypes;

    protected $request;

    public function __construct($routes = array(), $basePath = '', array $matchTypes = array())
    {
        $this->request = Everkit::app()->getComponent('request');
        $this->matchTypes = array(
            'i'  => '[0-9]++',
            'a'  => '[0-9A-Za-z]++',
            'h'  => '[0-9A-Fa-f]++',
            '*'  => '.+?',
            '**' => '.++',
            ''   => '[^/\.]++'
        );
        $this->addRoutes($routes);
        $this->setBasePath($basePath);
        $this->addMatchTypes($matchTypes);
    }

    public function getRoutes(): array
    {
        return $this->routes;
    }

    public function addRoutes($routes)
    {
        if (!is_array($routes) && !$routes instanceof Traversable) {
            throw new \Exception('Routes should be an array or an instance of Traversable');
        }
        foreach ($routes as $route) {
            call_user_func_array(array($this, 'map'), $route);
        }
    }

    public function setBasePath($basePath)
    {
        $this->basePath = $basePath;
    }

    public function addMatchTypes($matchTypes)
    {
        $this->matchTypes = array_merge($this->matchTypes, $matchTypes);
    }

    public function map($method, $route, $target, $name = null)
    {
        $this->routes[] = array($method, $route, $target, $name);
        if ($name) {
            if (isset($this->namedRoutes[$name])) {
                throw new \Exception("Can not redeclare route '{$name}'");
            }
            $this->namedRoutes[$name] = $route;
        }
    }

    public function generate($routeName, array $params = array())
    {
        if (!isset($this->namedRoutes[$routeName])) {
            throw new \Exception("Route '{$routeName}' does not exist.");
        }

        $route = $this->namedRoutes[$routeName];

        $url = $this->basePath . $route;

        if (preg_match_all(
            '`(/|\.|)\[([^:\]]*+)(?::([^:\]]*+))?\](\?|)`',
            $route,
            $matches,
            PREG_SET_ORDER
        )) {
            foreach ($matches as $index => list($block, $pre, $type, $param, $optional)) {
                if ($pre) {
                    $block = substr($block, 1);
                }
                if (isset($params[$param])) {
                    $url = str_replace($block, $params[$param], $url);
                } elseif ($optional && $index !== 0) {
                    $url = str_replace($pre . $block, '', $url);
                } else {
                    $url = str_replace($block, '', $url);
                }
            }
        }
        return $url;
    }

    public function match($requestUrl = null, $requestMethod = null)
    {
        /**
         * @var string $requestMethod
         */
        $result = false;
        $params = array();
        $requestServer = $this->request->server();

        if ($requestUrl === null && !empty($requestServer) && !empty($requestServer['REQUEST_URI'])) {
            $requestUrl = $requestServer['REQUEST_URI'];
        } elseif ($requestUrl === null) {
            $requestUrl = '/';
        }

        $requestUrl = substr($requestUrl, strlen((string)$this->basePath));

        if (($strpos = strpos($requestUrl, '?')) !== false) {
            $requestUrl = substr($requestUrl, 0, $strpos);
        }

        if ($requestMethod === null && !empty($requestServer) && !empty($requestServer['REQUEST_METHOD'])) {
            $requestMethod = $requestServer['REQUEST_METHOD'];
        } elseif ($requestMethod === null) {
            $requestMethod = 'GET';
        }

        foreach ($this->routes as list($methods, $route, $target, $name)) {
            $method_match = (stripos($methods, $requestMethod) !== false);
            $match = false;
            if (!$method_match) {
                continue;
            }
            if ($route === '*') {
                // * wildcard (matches all)
                $match = true;
            } elseif (isset($route[0]) && $route[0] === '@') {
                // @ regex delimiter
                $pattern = '`' . substr($route, 1) . '`u';
                $match = preg_match($pattern, $requestUrl, $params) === 1;
            } elseif (($position = strpos($route, '[')) === false) {
                // No params in url, do string comparison
                $match = strcmp($requestUrl, $route) === 0;
            } else {
                // Compare longest non-param string with url
                if (strncmp($requestUrl, $route, $position) !== 0) {
                    continue;
                }
                $regex = $this->compileRoute($route);
                $match = preg_match($regex, $requestUrl, $params) === 1;
            }

            if ($match) {
                if ($params) {
                    foreach ($params as $key => $value) {
                        if (preg_match('/^\d+$/', (string)$key)) {
                            unset($params[$key]);
                        }
                    }
                }
                $result = array(
                    'target' => $target,
                    'params' => $params,
                    'name' => $name
                );
            }
        }
        return $result;
    }

    private function compileRoute($route)
    {
        /**
         * @var array $matches
         */
        $result = null;
        if (preg_match_all(
            '`(/|\.|)\[([^:\]]*+)(?::([^:\]]*+))?\](\?|)`',
            $route,
            $matches,
            PREG_SET_ORDER
        )) {
            $matchTypes = $this->matchTypes;
            foreach ($matches as list($block, $pre, $type, $param, $optional)) {
                if (isset($matchTypes[$type])) {
                    $type = $matchTypes[$type];
                }
                if ($pre === '.') {
                    $pre = '\.';
                }

                $optional = $optional !== '' ? '?' : null;

                $pattern = '(?:'
                    . ($pre !== '' ? $pre : null)
                    . '('
                    . ($param !== '' ? "?P<$param>" : null)
                    . $type
                    . ')'
                    . $optional
                    . ')'
                    . $optional;

                $route = str_replace($block, $pattern, $route);
            }

        }
        $result = sprintf('`^%s`u', $route);
        return $result;
    }
}
