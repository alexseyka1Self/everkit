<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 03.05.17
 * Time: 15:59
 */

namespace Everkit\Framework\Classes;

use Everkit\Framework\Classes\Abstracts\ComponentAbstract;
use Everkit\Framework\Interfaces\IFlashBag;
use Everkit\Framework\Interfaces\IRenderable;
use Everkit\Framework\Interfaces\IWidget;
use Everkit\Framework\Everkit;
use Twig_Test;

class CTwig extends ComponentAbstract implements IRenderable
{

    public $templatesDir;
    public $cacheEnabled;
    public $cacheDir;
    public $fileExtension;
    public $viewsDir = APP_PATH . 'app/Views/';

    public function bootstrap(): bool
    {
        Everkit::app()->setup('view', $this);
        return parent::bootstrap();
    }

    public function render(string $file, array $data)
    {
        $loader = new \Twig_Loader_Filesystem([$this->templatesDir, $this->viewsDir], APP_PATH);
        $twig = new \Twig_Environment($loader, array(
            'cache' => $this->cacheEnabled ? $this->cacheDir : false,
        ));
        $this->addTests($twig);
        $this->addFunctions($twig);
        return $twig->render($this->getFileNameWithExt($file), $data);
    }

    private function getFileNameWithExt(string $filename)
    {
        return $filename . $this->fileExtension;
    }

    private function addTests(\Twig_Environment &$twig)
    {
        $test = new Twig_Test('currentURI', function ($value) {
            $result = false;
            $app = Everkit::app()->getComponent('request');
            $requestURI = $app->server()['REQUEST_URI'];
            if ($value === $requestURI
                || (preg_match('~'.addslashes($value).'~', $requestURI) && $value !== '/')) {
                return true;
            }
            return $result;
        });
        $twig->addTest($test);
    }

    private function addFunctions(\Twig_Environment &$twig)
    {
        $func = new \Twig_Function('isGuest', function () {
            $user = Everkit::app()->getComponent('user');
            return $user->isGuest();
        });
        $twig->addFunction($func);

        $func = new \Twig_Function('csrfToken', function () {
            $security = $this->app->getComponent('security');
            $string = sprintf(
                '<input type="hidden" name="_csrf_token" value="%s">',
                $security->getCsrfToken()
            );
            return $string;
        });
        $twig->addFunction($func);

        $func = new \Twig_Function('getFlashes', function () {
            $result = null;
            $session = $this->app->getComponent('session');
            if (!empty($session) && $session instanceof IFlashBag) {
                $result = $session->getFlashes();
            }
            return $result;
        });
        $twig->addFunction($func);

        $func = new \Twig_Function('startWidget', function (...$arguments) {
            $this->widgets('start', ...$arguments);
        });
        $twig->addFunction($func);
        $func = new \Twig_Function('widget', function (...$arguments) {
            $this->widgets('render', ...$arguments);
        });
        $twig->addFunction($func);
        $func = new \Twig_Function('endWidget', function (...$arguments) {
            $this->widgets('end', ...$arguments);
        });
        $twig->addFunction($func);
    }

    /**
     * @param string $method
     * @param array ...$arguments
     */
    protected function widgets(string $method = 'render', ...$arguments)
    {
        if (is_array($arguments)) {
            $widgetName = array_shift($arguments);
            $widgetParams = array_shift($arguments) ?? array();
            $widgetClass = $this->app->config->getWidgetClassName($widgetName);
            if (!empty($widgetClass)) {
                $widgetInstance = new $widgetClass($widgetParams);
                if ($widgetInstance instanceof IWidget) {
                    $widgetInstance->{$method}();
                }
            }
        }
    }
}
