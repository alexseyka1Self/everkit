<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 27.04.17
 * Time: 13:15
 */
declare(strict_types=1);
namespace Everkit\Framework\Interfaces;

interface ICommand
{
    public function runCommand(array $args);
}