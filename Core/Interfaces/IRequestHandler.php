<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 28.04.17
 * Time: 15:58
 */
declare(strict_types=1);
namespace Everkit\Framework\Interfaces;

/**
 * Interface IRequestHandler
 * @package Everkit\Framework\Interfaces
 */
interface IRequestHandler
{
    /**
     * Returns all get-parameters.
     * @param string $key
     * @return mixed
     */
    public function get(string $key);

    /**
     * Returns URI-string.
     * @return mixed
     */
    public function getUri();

    /**
     * Returns hostname-string.
     * @return mixed
     */
    public function getHost();

    /**
     * Returns used method (GET, POST or other).
     * @return mixed
     */
    public function getMethod();

    /**
     * Returns all query string.
     * @return mixed
     */
    public function getQueryString();

    /**
     * Returns query string in array.
     * @return mixed
     */
    public function getQueryArray();
}