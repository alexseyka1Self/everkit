<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 18.09.17
 * Time: 11:54
 */
declare(strict_types=1);
namespace Everkit\Framework\Interfaces;

interface IFlashBag
{
    public function getFlashes(): array;
    public function getFlash(string $key);
    public function setFlash(string $key, $value);
    public function hasFlash(string $key): bool;
}
