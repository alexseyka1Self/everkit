<?php
declare(strict_types=1);
namespace Everkit\Framework\Interfaces;

interface ISessionAttributes
{
    public function has(string $name): bool;
    public function get(string $name);
    public function set(string $name, $value);
    public function all(): array;
}