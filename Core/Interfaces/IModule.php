<?php

declare(strict_types=1);
namespace Everkit\Framework\Interfaces;

/**
 * Interface IModule
 * @package Everkit\Framework\Interfaces
 */
interface IModule
{
    /**
     * This method called before the control will be transferred to controller.
     * Application stops executing if its return false.
     * @param string $controller
     * @param string $action
     * @return mixed
     */
    public function beforeController(string $controller, string $action): bool;

    /**
     * @param string $controller
     * @param string $action
     * @return mixed
     */
    public function afterController(string $controller, string $action): bool;
}