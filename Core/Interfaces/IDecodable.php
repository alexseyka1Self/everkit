<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 27.04.17
 * Time: 12:41
 */
declare(strict_types=1);
namespace Everkit\Framework\Interfaces;

/**
 * Interface IDecodable
 * @package Everkit\Framework\Interfaces
 */
interface IDecodable
{
    /**
     * @param string $json
     * @return mixed
     */
    public static function decode(string $json);
}