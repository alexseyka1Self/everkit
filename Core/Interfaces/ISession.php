<?php

declare(strict_types=1);
namespace Everkit\Framework\Interfaces;

interface ISession
{
    public function start(): bool;
    public function isStarted(): bool;
    public function getSession(): array;
    public function getId(): string;
    public function setId(string $id): bool;
    public function getName(): string;
    public function setName(string $name): bool;
    public function destroy(): bool;
}