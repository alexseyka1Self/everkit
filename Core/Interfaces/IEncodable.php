<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 27.04.17
 * Time: 12:40
 */
declare(strict_types=1);
namespace Everkit\Framework\Interfaces;

/**
 * Interface IEncodable
 * @package Everkit\Framework\Interfaces
 */
interface IEncodable
{
    /**
     * @param $obj
     * @return mixed
     */
    public static function encode($obj);
}