<?php

declare(strict_types=1);
namespace Everkit\Framework\Interfaces;

/**
 * Interface IController
 * @package Everkit\Framework\Interfaces
 */
interface IController
{
    /**
     * This method called before the control will be transferred to action.
     * Application stops executing if its return false.
     * @param string $controller
     * @param string $action
     * @return bool
     */
    public function beforeAction(string $controller, string $action): bool;

    /**
     * This method called after action and it can store something to model (for example).
     * @param string $controller
     * @param string $action
     * @return bool
     */
    public function afterAction(string $controller, string $action): bool;
}
