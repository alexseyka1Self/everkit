<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 04.05.17
 * Time: 11:56
 */
declare(strict_types=1);
namespace Everkit\Framework\Interfaces;

/**
 * Dependency Injection Container
 * Interface IContainer
 * @package Everkit\Framework\Interfaces
 */
interface IContainer
{
    public function __set(string $key, $value);
    public function __get(string $key);
}