<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 05.05.17
 * Time: 17:49
 */
declare(strict_types=1);
namespace Everkit\Framework\Interfaces\Db;

interface ISqlCommand
{
    public function createCommand(string $sql);
    public function execute(): int;
}