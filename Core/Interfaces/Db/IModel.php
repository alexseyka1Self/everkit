<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 05.05.17
 * Time: 13:18
 */
declare(strict_types=1);
namespace Everkit\Framework\Interfaces\Db;

interface IModel
{
    public function find($condition = null);
    public function findAll($condition = null);
}