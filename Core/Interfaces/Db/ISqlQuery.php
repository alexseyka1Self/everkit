<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 05.05.17
 * Time: 17:51
 */
declare(strict_types=1);
namespace Everkit\Framework\Interfaces\Db;

interface ISqlQuery
{
    public function query();
    public function all();
    public function column();
    public function scalar();
}