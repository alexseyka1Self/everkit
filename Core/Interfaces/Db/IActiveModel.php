<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 11.09.17
 * Time: 15:21
 */
declare(strict_types=1);
namespace Everkit\Framework\Interfaces\Db;

interface IActiveModel
{
    public static function model();
    public function isNewRecord();
    public function relations();
    public function with();
    public function beforeFind();
    public function find($condition = null);
    public function afterFind(array $results);
    public function beforeSave(): bool;
    public function save();
    public function afterSave();
    public function getLastId();
    public function delete();
}