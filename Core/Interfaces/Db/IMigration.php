<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 04.09.17
 * Time: 13:59
 */
declare(strict_types=1);
namespace Everkit\Framework\Interfaces\Db;

interface IMigration
{
    public function up(): bool ;
    public function down(): bool;
}