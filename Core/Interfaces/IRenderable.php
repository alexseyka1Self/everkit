<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 03.05.17
 * Time: 13:06
 */
declare(strict_types=1);
namespace Everkit\Framework\Interfaces;

/**
 * Interface IRenderable used for render some content in HTML for showing to client.
 * @package Everkit\Framework\Interfaces
 */
interface IRenderable
{
    /**
     * Render php-template in HTML-code.
     * @param string $template
     * @param array $data
     * @return mixed
     */
    public function render(string $template, array $data);
}