<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 15.09.17
 * Time: 17:23
 */
declare(strict_types=1);
namespace Everkit\Framework\Interfaces;

interface IUserIdentity
{
    public function getLogin(): string;
    public function getPassword(): string;
}