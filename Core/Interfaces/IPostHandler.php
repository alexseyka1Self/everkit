<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 28.04.17
 * Time: 16:51
 */
declare(strict_types=1);
namespace Everkit\Framework\Interfaces;

interface IPostHandler
{
    public function post();
    public function getPost(string $key);
}