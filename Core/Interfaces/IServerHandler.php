<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 28.04.17
 * Time: 16:59
 */
declare(strict_types=1);
namespace Everkit\Framework\Interfaces;

interface IServerHandler
{
    public function server();
    public function getServer(string $key);
}