<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 27.04.17
 * Time: 13:15
 */
declare(strict_types=1);
namespace Everkit\Framework\Interfaces;

/**
 * Interface IComponent
 * The component will be connected to the Everkit system application in two stages:
 * 1) Method "bootstrap" used for registration your component in global visibility zone of application. This step occurs
 * before any request processing. Application just prepares his own workplace and registers himself in Dependency
 * Injection container ($this->app->setup()).
 * After registration you can turn to your components through the Everkit::app()->getComponent()
 * method or just $app->YOUR_COMPONENT_NAME (thanks to magic-methods).
 * 2) Method "run" is called during second iteration (after registration of all other components).
 * @package App\Core\Interfaces
 */
interface IComponent
{
    /**
     * Method used for registration your component in global visibility zone of application.
     * This step occurs before any request processing.
     * Application just prepares his own workplace and registers himself in Dependency
     * Injection container ($this->app->setup()).
     * After registration you can turn to your components through the Everkit::app()->getComponent()
     * method or just $app->YOUR_COMPONENT_NAME (thanks to magic-methods).
     * @return bool
     */
    public function bootstrap(): bool;

    /**
     * Method is called during second iteration (after registration of all other components).
     * @return bool
     */
    public function run(): bool;
}
