<?php
/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 20.09.17
 * Time: 16:48
 */

namespace Everkit\Framework\Interfaces;

/**
 * Class IWidget
 * @package Everkit\Framework\Interfaces
 */
interface IWidget
{
    /**
     * IWidget constructor.
     * @param array $params
     */
    public function __construct(array $params = array());
    /**
     *  Render first part if widget have a structure of block
     */
    public function start();

    /**
     * Render all widget template
     */
    public function render();
    /**
     *  Render last part if widget have a structure of block
     */
    public function end();
}
