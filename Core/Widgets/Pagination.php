<?php

namespace Everkit\Framework\Widgets;

use Everkit\Framework\Classes\Abstracts\WidgetAbstract;
use Everkit\Framework\Classes\Helpers\SimpleRenderer;

/**
 * Created by PhpStorm.
 * User: alexseyka1
 * Date: 20.09.17
 * Time: 16:47
 */
class Pagination extends WidgetAbstract
{
    /**
     * Elements per page
     * @var int
     */
    public $perPage = 10;
    public $header;

    /**
     *  Render first part if widget have a structure of block
     */
    public function start()
    {
        SimpleRenderer::render(__DIR__ . '/Views/alertStart.php', array('header' => $this->header));
    }

    /**
     * Render all widget template
     */
    public function render()
    {
        SimpleRenderer::render(__DIR__ . '/Views/pagination.php', array());
    }

    /**
     *  Render last part if widget have a structure of block
     */
    public function end()
    {
        echo '</div>';
    }
}
